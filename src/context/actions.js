import jwt_decode from "jwt-decode";
import { signIn } from "../api/auth";
import constants from "./constants";

export async function loginUser(dispatch, { email, password }) {
  let data = await signIn(email, password);
  console.log("data is ", data)
  try {
    if (data.code === 200) {
      const user = await getUserFromToken(data.token);
      dispatch({
        type: constants.LOGIN_SUCCESS,
        payload: { user, auth_token: data.token },
      });
      localStorage.setItem(
        "currentUser",
        JSON.stringify({ token: data.token })
      );
      console.log(" user is ", user)
      data.user = user;
      return data;
    }
    dispatch({ type: constants.LOGIN_ERROR, error: data.message });
    return;
  } catch (error) {
    dispatch({ type: constants.LOGIN_ERROR, error: error });
  }
}

export async function logout(dispatch) {
  dispatch({ type: constants.LOGOUT });
  localStorage.removeItem("currentUser");
}

export const getUserFromToken = (token) => {
  try {
    var decoded = jwt_decode(token);
    if (decoded.exp > Date.now() / 1000) {
      return decoded;
    } else return null;
  } catch (error) {
    console.log("the error is ", error);
    return null;
  }
};
