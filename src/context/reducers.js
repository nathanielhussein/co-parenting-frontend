import { getUserFromToken } from "./actions";
import constants from "./constants";

// let user = localStorage.getItem("currentUser")
//   ? JSON.parse(localStorage.getItem("currentUser")).user
//   : "";
let token = localStorage.getItem("currentUser")
  ? JSON.parse(localStorage.getItem("currentUser")).token
  : "";

export const initialState = {
  auth_user: "" || getUserFromToken(token),
  auth_token: "" || token,
  loading: false,
  errorMessage: null,
};
export const AuthReducer = (initialState, action) => {
  switch (action.type) {
    case constants.REQUEST_LOGIN:
      return {
        ...initialState,
        loading: true,
      };
    case constants.LOGIN_SUCCESS:
      return {
        ...initialState,
        user: action.payload.auth_user,
        token: action.payload.auth_token,
        loading: false,
      };
    case constants.LOGOUT:
      return {
        ...initialState,
        auth_user: "",
        auth_token: "",
      };

    case constants.LOGIN_ERROR:
      return {
        ...initialState,
        loading: false,
        errorMessage: action.error,
      };

    default:
      throw new Error(`Unhandled action type: ${action.type}`);
  }
};
