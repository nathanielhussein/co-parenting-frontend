import { api } from "api";

export const getAllUsers = async () => {
  try {
    const { data } = await api.get("/users");
    if (data.code === 200) {
      return data.data;
    }
    return null;
  } catch (error) {
    console.log("error is ", error.response.data);
    return error.response.data;
  }
};

export const getAllMembers = async (page_number, limit) => {
  try {
    const { data } = await api.get(`/users/team-members?page-number=${page_number}&items-per-page=${limit}`);
    if (data.code === 200) {
      console.log("data is ", data);
      return data.data;
    }
    return null;
  } catch (error) {
    console.log("error is ", error.response.data);
    return error.response.data;
  }
};

export const getRoleTeamMembers = async (roleName) => {
  try {
    console.log("the role is ", roleName.toUpperCase());
    const { data } = await api.get(
      `/users/users-by-role?role=${roleName.toUpperCase()}`
    );
    if (data.code === 200) {
      console.log("data is ", data);

      return data.data;
    }
    return null;
  } catch (error) {
    console.log("error is ", error.response.data);
    return error.response.data;
  }
};

export const getAccessGroupList = async () => {
  try {
    const { data } = await api.get("/auth/roles");
    console.log("roles are ", data)
    if (data.code === 200) {
      return data.data;
    }
    return null;
  } catch (error) {
    console.log("error is ", error.response.data);
    return error.response.data;
  }
};

export const deleteAccessGroup = async (roleName) => {
  try {
    const { data } = await api.delete(`/auth/roles/${roleName}`);
    if (data.code === 200) {
      return data.data;
    }
    return null;
  } catch (error) {
    console.log("error is ", error.response.data);
    return error.response.data;
  }
};

export const createNewAccessGroup = async (accessGroupName) => {
  try {
    const { data } = await api.post("/auth/roles", {
      roleName: accessGroupName,
    });
    console.log("data is ", data);
    if (data.sucess) {
      return data.sucess;
    }
    return null;
  } catch (error) {
    console.log("error is ", error.response.data);
    return error.response.data;
  }
};

export const updateAccessGroup = async (role_id, accessGroupName) => {
  try {
    const { data } = await api.patch(`/auth/roles/${role_id}`, {
      roleName: accessGroupName,
    });
    console.log("data is ", data);
    if (data.sucess) {
      return data.sucess;
    }
    return null;
  } catch (error) {
    console.log("error is ", error.response.data);
    return error.response.data;
  }
};

export const inviteNewMember = async (
  firstName,
  lastName,
  email,
  password,
  role
) => {
  try {
    const { data } = await api.post("/auth/signup-admin", {
      firstName,
      lastName,
      email,
      password,
      userRole: role,
    });
    if (data.code === 200) {
      return data;
    }
    return null;
  } catch (error) {
    console.log(error.response.data);
    return error.response.data;
  }
};

export const editTeamMember = async (userId, firstName, lastName, email, role) => {
  try {
    const { data } = await api.patch(`/users/${userId}`, {
      firstName,
      lastName,
      email,
      userRole: role,
    });
    if (data.sucess) {
      return data;
    }
    return null;
  } catch (error) {
    console.log(error.response.data);
    return error.response.data;
  }
};

export const deleteTeamMember = async (userId) => {
  try {
    const { data } = await api.delete(`/users/${userId}`);
    if (data.code === 200) {
      return data.data;
    }
    return null;
  } catch (error) {
    console.log("error is ", error.response.data);
    return error.response.data;
  }
};
