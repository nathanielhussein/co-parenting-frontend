import { api } from "../";

export const getEveryUsers = async () => {
  try {
    const { data } = await api.get("/users");
    if (data.code === 200) {
      return data.data;
    }
    return null;
  } catch (error) {
    console.log("error is ", error.response.data);
    return error.response.data;
  }
};
export const getAllUsers = async () => {
  try {
    const { data } = await api.get("/users/team-members");
    if (data.code === 200) {
      return data.data;
    }
    return null;
  } catch (error) {
    console.log("error is ", error.response.data);
    return error.response.data;
  }
};

export const getChildren = async (connectionId) => {
  let result = [];
  try {
    const { data } = await api.get(`/children?connectionId=${connectionId}`);
    if (data.data) {
      result = data.data;
    }
    return result;
  } catch (error) {
    // console.log("error is ", error.response.data);
    // return [];
  }
  return result;
};

export const getUserData = async (id) => {
  try {
    let user;
    const { data: userData } = await api.get(`/users/${id}`);

    if (userData.code === 200) {
      user = userData.data;
      console.log("fetched user is ", user);
      return userData.data
    }
    console.log("users are", user);
    return null;
  } catch (error) {
    console.log("there is also another error");
    return null;

  }
};

export const getUserDetails = async (id) => {
  try {
    let user;
    const {data: userData } = await api.get(`/users/${id}`);

    if(userData.code === 200){
      user = userData.data;
      console.log("fetched user is ", user)
    }
    console.log("users are", user);
  
    const { data } = await api.get(`/connections?id=${id}`)
    if (data.data) {
      let connections = [];
      if (data.data.sent) {
        connections = [...connections, ...data.data.sent];
      }
      if (data.data.received) {
        connections = [...connections, ...data.data.received];
      }
      let userDetail = {
        id: user.id,
        userEmail: user.email,
        username: `${user.firstName} ${user.lastName}`,
        coparents: [],
      };
      for (let connection of connections) {
        console.log("connection ", connection);
        let coparentData = {
          connectionId: "",
          name: "",
          email: "",
          children: [],
        };
        coparentData.connectionId = connection.id;
        coparentData.name = connection.receiverName;
        coparentData.email = connection.requestReceiver;
        const childrenData = await getChildren(connection.id);
        if (childrenData) {
          coparentData.children = childrenData;
        }
        userDetail.coparents.push(coparentData);
      }
      return userDetail;
    }
  } catch (error) {
    console.log("there is also another error");
  }
};

export const getUserHistory = async () => {
  try {
    const { data } = await api.get("/users");
    if (data.code === 200) {
      return data.data;
    }
    return null;
  } catch (error) {
    console.log("error is ", error.response.data);
    return error.response.data;
  }
};

export const getUserExpenses = async (connectionId) => {
  try {
    const { data } = await api.get(`/expenses?connectionId=${connectionId}`);
    console.log("data is ", data);
    if (data.sucess) {
      return data.data;
    }
    return null;
  } catch (error) {
    console.log("error is ", error.response.data);
    return [];
  }
};

export const getUserEvents = async (connectionId) => {
  try {
    const { data } = await api.get(
      `/events?connectionId=eb1f6789-06e5-4b53-a68f-85e92619723a`
    );
    console.log("data is ", data);
    if (data.sucess) {
      return data.data;
    }
    return null;
  } catch (error) {
    console.log("error is ", error.response.data);
    return [];
  }
};

export const addNewUser = async (
  firstName,
  lastName,
  email,
  password,
  roleId
) => {
  try {
    const { data } = await api.post("/auth/signup", {
      firstName,
      lastName,
      email,
      password,
    });
    return data;
  } catch (error) {
    console.log(error.response.data);
    return error.response.data;
  }
};

export const updateUser = async (
  id,
  firstName,
  lastName,
  email,
  password,
) => {
  try {
    const { data } = await api.patch(`/users/${id}`, {
      firstName,
      lastName,
      email,
      password: password ? password : null,
    });
    return data;
  } catch (error) {
    console.log(error.response.data);
    return error.response.data;
  }
};

export const deleteUser = async (email) => {
  try {
    const { data } = await api.delete(`/user/${email}`);
    return data;
  } catch (error) {
    console.log(error.response.data);
    return error.response.data;
  }
};
