import { api } from "../";

export const signIn = async (email, password) => {
  try {
    const { data } = await api.post("/auth/login", {
      email,
      password,
    });
    return data;
  } catch (error) {
    console.log(error.response.data);
    return error.response.data;
  }
};

export const signUp = async (email, password) => {
  try {
    const { data } = await api.post("/auth/signup", {
      email,
      password,
    });
    console.log({ data });
    return data;
  } catch (error) {
    console.log(error.response.data);
    return error.response.data;
  }
};

export const forgotPassword = async (email) => {
  try {
    const { data } = await api.post("/auth/forgot", { email });
    if (data) {
      return data;
    }
  } catch (error) {
    return error.response;
  }
};

export const resetPassword = async (email, resetCode, password) => {
  try {
    const data = await api.post("/auth/reset", { email, resetCode, password });
    if (data) {
      return data;
    }
  } catch (error) {
    return error.response;
  }
};

export const resendOTP = async (email) => {
  try {
    const data = await api.post("/auth/resend", { email});
    if (data) {
      return data;
    }
  } catch (error) {
    return error.response;
  }
};
