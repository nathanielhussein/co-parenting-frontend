import axios from "axios";
import axiosRetry from "axios-retry";

const API_ROOT =  process.env.REACT_APP_API_ROOT || "http://localhost:3000/dev";

let token = localStorage.getItem("currentUser")
  ? JSON.parse(localStorage.getItem("currentUser")).token
  : "";

const api = axios.create({
  baseURL: API_ROOT,
  headers: { Authorization: `Bearer ${token}` },
});

axiosRetry(api, {
  retries: 3,
});

export { api };
