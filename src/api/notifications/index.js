import { api } from "api";

export const sendNotification = async ({ usersId, title, description }) => {
  try {
    let promises = [];
    for (let userId of usersId) {
      promises.push(
        api.post(`/notifications`, {
          userId,
          title,
          description,
        })
      );
    }
    const allResults = await Promise.all(promises);
    return allResults[0];
  } catch (error) {
    console.log(error.response.data);
    return error.response.data;
  }
};

export const getAllNotifications = async (currentPage, limit) => {
  try {
    const {data} = await api.get(`/notifications/all-notifications?page-number=${currentPage}&items-per-page=${limit}`);
    console.log("data is", data )
    return data;
  } catch (error) {
    console.log(error.response.data);
    return error.response.data;
  }
};

export const deleteNotifications = async (id) => {
  try {
    const { data } = await api.delete(`/notifications/${id}`);
    return data;
  } catch (error) {
    console.log(error.response.data);
    return error.response.data;
  }
};
