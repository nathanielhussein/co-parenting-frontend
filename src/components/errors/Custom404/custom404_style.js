import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    position: "relative",
    height: "100%",
    display: "grid",
    placeContent: "center",
    textAlign: "center",
    
  },

  title: {
    fontFamily: "Fredoka One, cursive",
    fontSize: "180px",
    margin: "0px",
    color: theme.palette.primary,
    textTransform: "uppercase",
    display: "inline-block",
    fontWeight: "700",
  },
  subtitle: {
    fontFamily: "Raleway",
    fontSize: "22px",
    fontWeight: "400",
    textTransform: "uppercase",
    margin: "0",
  },
  notfoundLink: {
    fontFamily: "Raleway",
    display: "inline-block",
    fontWeight: "700",
    borderRadius: "15px",
    color: theme.palette.primary,
  },
  returnarrow: {
    position: "relative",
    top: "-2px",
    color: theme.palette.primary,
    borderWidth: "0 3px 3px 0",
    display: "inline-block",
    padding: "3px",
    fontSize: "20px",
    "&:before": {
      content: "",
      border: "3px",
    },
  },
}));
export default useStyles;
