import { Typography } from "@material-ui/core";
import { Link } from "react-router-dom";
import useStyles from "./custom404_style";

function Custom404() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div className={classes.notfound}>
        <div className={classes.notfound404}>
          <Typography className={classes.title} variant="h1" color="primary">
            404
          </Typography>
        </div>
        <Typography className={classes.subtitle} variant="h4" color="primary">
          Oops, The Page you are looking for can't be found!
        </Typography>
        <Link to="/" className={classes.notfoundLink}>
          <Typography className={classes.returnarrow}></Typography>
          <Typography variant="subtitle1" color="textSecondary">Return To Homepage</Typography>
        </Link>
      </div>
    </div>
  );
}

export default Custom404;
