import React from "react";
import Backdrop from "@material-ui/core/Backdrop";
import Grid from "@material-ui/core/Grid";
// import HashLoader from "react-spinners/HashLoader";
import SyncLoader from "react-spinners/SyncLoader";
export const GlobalLoader = ({ size = 25, color = "#30637a" }) => {
  return (
    <Backdrop invisible open={true}>
      <Grid container alignItems="center" justify="center">
        <SyncLoader size={size} color={color} />
      </Grid>
    </Backdrop>
  );
};

export const ButtonLoader = ({ size = 15, color = "#30637a" }) => {
  return <SyncLoader size={size} color={color} />;
};

export const NormalLoader = ({ size = 15, color = "#30637a" }) => {
  return <SyncLoader size={size} color={color} />;
};
