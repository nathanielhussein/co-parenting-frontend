import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  form: {
    padding: "20px",
    position: "relative",
  },
  input: {
    backgroundColor: "#eee",
    borderRadius: "5px",
  },
  personIcon: {
    color: "rgba(0, 255, 0, 0.7)",
  },
  header_actions_addBtn: {
    minWidth: "150px",
    borderRadius: "50px",
  },
  button: {
    borderRadius: "20px",
  },
  closeBtn: {
    width: "300px",
    padding: "10px 20px",
  },

  btn: {
    textTransform: "none",
    width: "150px",
    borderRadius: "20px",
  },
  continueBtn: {
    width: "200px",
    padding: "10px 20px",
  },
  successIcon: {
    height: "100px",
    width: "100px",
  },
}));

export default useStyles