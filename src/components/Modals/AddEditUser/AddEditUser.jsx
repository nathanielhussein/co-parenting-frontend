import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import {
  Box,
  Divider,
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  OutlinedInput,
  Typography,
} from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";

import ClearIcon from "@material-ui/icons/Clear";
import useStyles from "./addedituser_style";
import clsx from "clsx";
import ConfirmationDialog from "../ConfirmationDialog/ConfirmationDialog";
export default function AddEditUser({ userData = null }) {
  const classes = useStyles();
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [open, setOpen] = useState(false);
  const [isOperationDone, setIsOperationDone] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setIsOperationDone(true);
  };

  return (
    <div>
      {userData !== null ? (
        <IconButton
          onClick={handleClickOpen}
          color="primary"
          style={{ border: "1px solid rgba(0, 0, 0, 0.25)" }}
        >
          <EditIcon color="primary" />
        </IconButton>
      ) : (
        <Button
          className={classes.header_actions_addBtn}
          variant="contained"
          color="primary"
          onClick={handleClickOpen}
        >
          {userData === null ? "Add" : "Edit"}
        </Button>
      )}
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <form className={classes.form}>
          <Grid spacing={2} container>
            <Grid
              item
              xs={12}
              container
              alignItems="center"
              justify="space-between"
            >
              <Grid item xs={10}>
                <Typography variant="h4" align="center" color="primary">
                  <strong>
                    {userData === null
                      ? "Add Team Member"
                      : "Edit User information"}
                  </strong>
                </Typography>
                {userData !== null && (
                  <Typography
                    align="center"
                    variant="subtitle1"
                    color="primary"
                  >
                    All changes will have immediate effect
                  </Typography>
                )}
              </Grid>

              <Grid item xs={2}>
                <Box display="flex" justifyContent="flex-end">
                  <IconButton onClick={() => setOpen(false)}>
                    <ClearIcon />
                  </IconButton>
                </Box>
              </Grid>
            </Grid>

            <Grid item xs={12}>
              <Divider light />
            </Grid>
            <Grid item xs={12}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="firstName">
                  <Typography>First Name</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="filled"
                  style={{ width: "100%" }}
                >
                  <OutlinedInput
                    id="firstName"
                    name="firstName"
                    placeholder="Enter First Name"
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                  />
                </FormControl>
              </Box>
            </Grid>

            <Grid item xs={12}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="lastName">
                  <Typography>Lastname</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="filled"
                  style={{ width: "100%" }}
                >
                  <OutlinedInput
                    id="lastName"
                    name="lastName"
                    placeholder="Enter Last Name"
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                  />
                </FormControl>
              </Box>
            </Grid>

            <Grid item xs={12}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="email">
                  <Typography>Email Address</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="filled"
                  style={{ width: "100%" }}
                >
                  <OutlinedInput
                    id="email"
                    name="email"
                    placeholder="Enter Email here"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </FormControl>
              </Box>
            </Grid>

            <Grid item xs={12}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="password">
                  <Typography>Password</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="filled"
                  style={{ width: "100%" }}
                >
                  <OutlinedInput
                    id="password"
                    name="password"
                    type="password"
                    placeholder="Enter Password here"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </FormControl>
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="confirm">
                  <Typography>Confirm Password</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="filled"
                  style={{ width: "100%" }}
                >
                  <OutlinedInput
                    id="confirm"
                    name="confirm"
                    type="password"
                    placeholder="Enter Confirm Password here"
                    value={confirmPassword}
                    onChange={(e) => setConfirmPassword(e.target.value)}
                  />
                </FormControl>
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Divider light />
            </Grid>
            <Grid item xs={12} justify="center" container>
              {userData === null ? (
                <>
                  <Box mx={2}>
                    <Button
                      color="secondary"
                      variant="contained"
                      className={clsx(classes.button, classes.addMoreBtn)}
                      onClick={handleClose}
                    >
                      Add More
                    </Button>
                  </Box>
                  <Box>
                    <Button
                      color="primary"
                      variant="contained"
                      className={clsx(classes.button, classes.sendInviteBtn)}
                      onClick={handleClose}
                    >
                      Send Invite
                    </Button>
                  </Box>
                </>
              ) : (
                <Box>
                  <Button
                    color="primary"
                    variant="contained"
                    fullWidth
                    className={clsx(classes.btn, classes.closeBtn)}
                    onClick={handleClose}
                  >
                    Save
                  </Button>
                </Box>
              )}
            </Grid>
          </Grid>
        </form>
      </Dialog>
      <ConfirmationDialog
        buttonText="Continue"
        func={() => {
          console.log("bye bye");
        }}
        isOperationDone={isOperationDone}
        setIsOperationDone={setIsOperationDone}
        message="The user information has been updated successfully"
      />
    </div>
  );
}
