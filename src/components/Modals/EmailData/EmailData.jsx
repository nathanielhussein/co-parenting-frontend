import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import {
  Box,
  Divider,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  InputLabel,
  OutlinedInput,
  Typography,
} from "@material-ui/core";
import ClearIcon from "@material-ui/icons/Clear";
import useStyles from "./emaildata_style";
import clsx from "clsx";
import MailOutlineOutlinedIcon from "@material-ui/icons/MailOutlineOutlined";
import ConfirmationDialog from "../ConfirmationDialog/ConfirmationDialog";
import { useFormik } from "formik";
import * as Yup from "yup";
import { ButtonLoader } from "../../Loaders/Loader";

export default function EmailData() {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [isEmailHasSent, setIsEmailHasSent] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setIsEmailHasSent(true);
  };

  const schema = Yup.object().shape({
    email: Yup.string().required("Required").email("Email Must be Valid!"),
  });
  const formik = useFormik({
    initialValues: {
      email: "",
    },
    validationSchema: schema,
    onSubmit: async (values) => {
      try {
        setLoading(true);
        // const data = await resetPassword(dispatch, { values });
        // console.log("data is ", data);
        // if (data.user) {
        //   history.go("/dashboard");
        //   return;
        // }
      } catch (error) {}
      setTimeout(() => {
        handleClose();
        setLoading(false);
      }, 1000);
    },
  });

  return (
    <div>
      <IconButton onClick={handleClickOpen}>
        <MailOutlineOutlinedIcon />
      </IconButton>

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <form className={classes.form} onSubmit={formik.handleSubmit}>
          <Grid>
            <Box display="flex" justifyContent="flex-end">
              <IconButton onClick={(e) => setOpen(false)}>
                <ClearIcon />
              </IconButton>
            </Box>
          </Grid>
          <Grid spacing={3} container justify="center">
            <Grid item xs={10}>
              <Typography align="center" color="primary" variant="h4">
                <strong>Email Data</strong>
              </Typography>
              <Typography color="primary" align="center">
                The information will be sent to your email
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Divider light />
            </Grid>
            <Grid item xs={10}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="email">
                  <Typography>Email Address</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="filled"
                  style={{ width: "100%" }}
                >
                  <OutlinedInput
                    id="email"
                    name="email"
                    placeholder="Enter Email here"
                    onChange={formik.handleChange}
                    value={formik.values.email}
                    error={formik.errors.email}
                  />
                  <FormHelperText style={{ color: "red" }}>
                    {formik.touched.email && formik.errors.email ? (
                      <div>{formik.errors.email}</div>
                    ) : null}
                  </FormHelperText>
                </FormControl>
              </Box>
            </Grid>
            <Grid item xs={10} justify="center" container>
              <Button
                color="primary"
                variant="contained"
                fullWidth
                type="submit"
                className={clsx(classes.btn, classes.continueBtn)}
              >
                {loading ? <ButtonLoader color="#ffffff" size={10} /> : "Send"}
              </Button>
            </Grid>
          </Grid>
        </form>
      </Dialog>
      <ConfirmationDialog
        buttonText="Continue"
        func={() => {
          console.log("bye bye");
        }}
        isOperationDone={isEmailHasSent}
        setIsOperationDone={setIsEmailHasSent}
        message={`The Data has been sent to: <br /><strong>${formik.values.email}</strong>`}
      />
    </div>
  );
}
