import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  form: {
    padding: "20px",
    position: "relative",
  },
  input: {
    backgroundColor: "#eee",
    borderRadius: "5px",
  },
  header_actions_addBtn: {
    minWidth: "100px",
    borderRadius: "50px",
  },
  btn: {
    textTransform: "none",
    width: "100px",
    borderRadius: "20px",
  },
  continueBtn: {
    width: "300px",
    padding: "10px 20px",
  },
  successIcon: {
    height: "100px",
    width: "100px",
  },
}));

export default useStyles;
