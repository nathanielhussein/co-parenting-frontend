import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DeleteOutlineOutlinedIcon from "@material-ui/icons/DeleteOutlineOutlined";

import { Box, Grid, IconButton, Typography } from "@material-ui/core";
import ClearIcon from "@material-ui/icons/Clear";
import useStyles from "./deleteuser_style";
import clsx from "clsx";
import { deleteUser } from "api/users";
import { GlobalLoader } from "components/Loaders/Loader";
export default function DeleteUser({ userEmail }) {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleDelete = async () => {
    try {
      setLoading(true);
      const data = await deleteUser(userEmail);
      if (data) {
        console.log("user deleted successfully");
      }
    } catch (error) {
      console.log("there is an error on deleting user", error);
    }
    setLoading(false);
    handleClose();
  };

  return (
    <div>
      <Button
        variant="outlined"
        size="small"
        className={clsx(classes.btn, classes.deleteBtn)}
        startIcon={<DeleteOutlineOutlinedIcon color="error" />}
        onClick={handleClickOpen}
      >
        <Typography color="error" variant="subtitle1">
          Delete
        </Typography>
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <form className={classes.form}>
          <Grid container justify="center">
            <Grid item xs={12} container alignItems="center" justify="flex-end">
              <IconButton onClick={handleClose}>
                <ClearIcon />
              </IconButton>
            </Grid>
            <Grid item xs={10}>
              <Box>
                <Typography variant="h4" color="primary" align="center">
                  <strong>Delete Member</strong>
                </Typography>
              </Box>
            </Grid>

            <Grid item xs={10}>
              <Box my={3}>
                <Typography align="center">
                  Are you sure want to delete this user?
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={12} justify="center" container>
              {loading === false ? (
                <>
                  <Box mx={2}>
                    <Button
                      //   color="secondary"
                      variant="outlined"
                      className={clsx(classes.btn, classes.yesBtn)}
                      onClick={handleDelete}
                    >
                      Yes
                    </Button>
                  </Box>
                  <Box>
                    <Button
                      color="primary"
                      variant="contained"
                      className={clsx(classes.btn, classes.noBtn)}
                      onClick={handleClose}
                    >
                      No
                    </Button>
                  </Box>
                </>
              ) : (
                <GlobalLoader />
              )}
            </Grid>
          </Grid>
        </form>
      </Dialog>
    </div>
  );
}
