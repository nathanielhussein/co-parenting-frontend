import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  form: {
    padding: "20px",
    position: "relative",
  },

  btn: {
    textTransform: "none",
    width: "150px",
    borderRadius: "20px",
  },
  yesBtn: {
    // backgroundColor: "rgba(28, 173, 40, 0.3)",
    // color: theme.palette.primary,
  },
  noBtn: {
    // backgroundColor: "rgba(255, 0, 0, 0.2)",
  },
}));

export default useStyles;
