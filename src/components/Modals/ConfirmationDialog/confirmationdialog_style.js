import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  form: {
    padding: "20px",
    position: "relative",
  },

  btn: {
    textTransform: "none",
    // width: "200px",
    borderRadius: "20px",
  },
  button: {
    borderRadius: "20px",
  },
  continueBtn: {
    // width: "200px",
    padding: "10px 20px",
  },
  successIcon: {
    height: "100px",
    width: "100px",
  },
}));

export default useStyles;
