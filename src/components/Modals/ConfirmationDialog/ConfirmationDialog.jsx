import React from "react";
import useStyles from "./confirmationdialog_style";
import ClearIcon from "@material-ui/icons/Clear";
import {
  Box,
  Button,
  Dialog,
  Grid,
  IconButton,
  Typography,
} from "@material-ui/core";
import clsx from "clsx";

function ConfirmationDialog({
  message,
  buttonText,
  isOperationDone,
  setIsOperationDone,
  func,
}) {
  const classes = useStyles();

  const handleContinue = () => {
    setIsOperationDone(false);
    func();
  };
  return (
    <>
      <Dialog
        open={isOperationDone}
        onClose={() => setIsOperationDone(false)}
        aria-labelledby="form-dialog-title"
      >
        <form className={classes.form}>
          <Grid container alignItems="center" justify="flex-end">
            <IconButton onClick={() => setIsOperationDone(false)}>
              <ClearIcon />
            </IconButton>
          </Grid>
          <Grid container justify="center" spacing={3}>
            <Grid item xs={10}>
              <Box mb={2} display="flex" justifyContent="center">
                <img
                  className={classes.successIcon}
                  src="/images/success_icon.svg"
                  alt="success icon"
                />
              </Box>
            </Grid>

            <Grid item xs={10}>
              <Box>
                <Typography color="primary" variant="h5" align="center">
                  <strong dangerouslySetInnerHTML={{ __html: message }} />
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={6} justify="center" container>
              <Button
                fullWidth
                color="primary"
                variant="contained"
                className={clsx(classes.btn, classes.continueBtn)}
                onClick={handleContinue}
              >
                {buttonText}
              </Button>
            </Grid>
          </Grid>
        </form>
      </Dialog>
    </>
  );
}

export default ConfirmationDialog;
