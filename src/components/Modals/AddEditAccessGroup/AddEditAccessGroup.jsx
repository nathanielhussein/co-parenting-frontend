import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";

import {
  Box,
  Divider,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  InputLabel,
  OutlinedInput,
  Typography,
} from "@material-ui/core";
import ClearIcon from "@material-ui/icons/Clear";
import useStyles from "./addeditaccessgroup_style";
import clsx from "clsx";
import ConfirmationDialog from "../ConfirmationDialog/ConfirmationDialog";
import { useFormik } from "formik";
import * as Yup from "yup";
import { ButtonLoader } from "components/Loaders/Loader";
import {
  createNewAccessGroup,
  updateAccessGroup,
} from "api/teams";

export default function AddEditAccessGroup({
  isEditing = false,
  accessGroup = null,
}) {
  // console.log("access group is ", accessGroup.roleName);
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [isOperationDone, setIsOperationDone] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setIsOperationDone(true);
  };

  const schema = Yup.object().shape({
    roleName: Yup.string()
      .min(4, "Must be 4 characters or more")
      .required("Required"),
  });
  const formik = useFormik({
    initialValues: {
      roleName: accessGroup !== null ? accessGroup.roleName : "",
    },
    validationSchema: schema,
    onSubmit: async (values) => {
      try {
        setLoading(true);
        let data;
        if (isEditing) {
          data = await updateAccessGroup(accessGroup.id, values.roleName);
        } else {
          data = await createNewAccessGroup(values.roleName);
        }
        console.log("data is ", data);
        if (data) {
          handleClose();
        }
      } catch (error) {
        console.log("there is an error on creating access group ", error);
      }
      setLoading(false);
    },
  });

  return (
    <div>
      <Button
        className={classes.header_actions_addBtn}
        variant="contained"
        color="primary"
        onClick={handleClickOpen}
      >
        {accessGroup ? "Edit Group" : "Add New Group"}
      </Button>
      <Dialog
        open={open}
        onClose={() => setOpen(false)}
        aria-labelledby="form-dialog-title"
      >
        <form className={classes.form} onSubmit={formik.handleSubmit}>
          <Grid spacing={2} container justify="center">
            <Grid
              item
              xs={12}
              container
              alignItems="center"
              justify="space-between"
            >
              <Grid item xs={10}>
                <Typography align="center" variant="h4" color="primary">
                  {!isEditing ? "Create Access Group" : "Edit Access Group"}
                </Typography>
                <Typography align="center" variant="subtitle1" color="primary">
                  Please provide the following information:
                </Typography>
              </Grid>

              <Grid item xs={2}>
                <Box my={1} display="flex" justifyContent="flex-end">
                  <IconButton onClick={() => setOpen(false)}>
                    <ClearIcon />
                  </IconButton>
                </Box>
              </Grid>
            </Grid>

            <Grid item xs={12}>
              <Divider light />
            </Grid>
            <Grid item xs={10}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="access_group_title">
                  <Typography>Access group title</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="filled"
                  style={{ width: "100%" }}
                >
                  <OutlinedInput
                    id="access_group_title"
                    name="roleName"
                    placeholder="Enter title here"
                    error={formik.errors && Boolean(formik.errors.roleName)}
                    onChange={formik.handleChange}
                    value={formik.values.roleName}
                  />
                  <FormHelperText style={{ color: "red" }}>
                    {formik.touched.roleName && formik.errors.roleName ? (
                      <div>{formik.errors.roleName}</div>
                    ) : null}
                  </FormHelperText>
                </FormControl>
              </Box>
            </Grid>
            <Grid item xs={10}>
              <Divider light />
            </Grid>
            <Grid item xs={10} justify="center" container>
              <Box width="60%">
                <Button
                  type="submit"
                  color="primary"
                  variant="contained"
                  fullWidth
                  className={clsx(classes.btn, classes.closeBtn)}
                >
                  {loading ? (
                    <ButtonLoader color="#ffffff" size={10} />
                  ) : accessGroup ? (
                    "Save"
                  ) : (
                    "Create"
                  )}
                </Button>
              </Box>
            </Grid>
          </Grid>
        </form>
      </Dialog>
      <ConfirmationDialog
        buttonText="Continue"
        func={() => {
          // console.log("bye bye");
        }}
        isOperationDone={isOperationDone}
        setIsOperationDone={setIsOperationDone}
        message={
          isEditing
            ? "The AccessGroup Name is Changed successfully"
            : "The new group added successfully"
        }
      />
    </div>
  );
}
