import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";

import {
  Box,
  Divider,
  FilledInput,
  FormControl,
  Grid,
  IconButton,
  InputBase,
  InputLabel,
  MenuItem,
  OutlinedInput,
  Select,
  Typography,
} from "@material-ui/core";
import ClearIcon from "@material-ui/icons/Clear";
import useStyles from "./addgroupmember_style";
import clsx from "clsx";
import ConfirmationDialog from "../ConfirmationDialog/ConfirmationDialog";
export default function AddGroupMember({ userData = null }) {
  const classes = useStyles();
  const [role, setRole] = useState(0);
  const [accessGroup, setAccessGroup] = useState(0);
  const [email, setEmail] = useState("");
  const [open, setOpen] = useState(false);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [isOperationDone, setIsOperationDone] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setIsOperationDone(true);
  };

  return (
    <div>
      <Button
        className={classes.header_actions_addBtn}
        variant={userData === null ? "contained" : "outlined"}
        color="primary"
        onClick={handleClickOpen}
      >
        {userData === null ? "Invite" : "Edit"}
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <form className={classes.form}>
          <Grid spacing={2} container>
            <Grid
              item
              xs={12}
              container
              alignItems="center"
              justify="space-between"
            >
              <Grid item xs={10}>
                <Typography variant="h4" color="primary">
                  {userData === null ? "Invite Team Member" : "Edit User"}
                </Typography>
                <Typography variant="subtitle1" color="primary">
                  Please provide the following information:
                </Typography>
              </Grid>

              <Grid item xs={2}>
                <Box display="flex" justifyContent="flex-end">
                  <IconButton onClick={handleClose}>
                    <ClearIcon />
                  </IconButton>
                </Box>
              </Grid>
            </Grid>

            <Grid item xs={12}>
              <Divider light />
            </Grid>
            <Grid item xs={12}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="firstName">
                  <Typography>First Name</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="filled"
                  style={{ width: "100%" }}
                >
                  <OutlinedInput
                    id="firstName"
                    name="firstName"
                    placeholder="Enter First Name"
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                  />
                </FormControl>
              </Box>
            </Grid>

            <Grid item xs={12}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="lastName">
                  <Typography>Lastname</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="filled"
                  style={{ width: "100%" }}
                >
                  <OutlinedInput
                    id="lastName"
                    name="lastName"
                    placeholder="Enter Last Name"
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                  />
                </FormControl>
              </Box>
            </Grid>

            <Grid item xs={12}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="email">
                  <Typography>Email Address</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="filled"
                  style={{ width: "100%" }}
                >
                  <OutlinedInput
                    id="email"
                    name="email"
                    placeholder="Enter Email here"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </FormControl>
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="role">
                  <Typography>Role</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="outlined"
                  style={{ width: "100%" }}
                >
                  <Select
                    labelId="demo-customized-select-label"
                    id="demo-customized-select"
                    value={role}
                    placeholder="select Group"
                    onChange={(e) => setRole(e.target.value)}
                  >
                    <MenuItem value={0}>
                      <em>Select a role</em>
                    </MenuItem>
                    <MenuItem value={1}>Admin</MenuItem>
                    <MenuItem value={2}>Supervisor</MenuItem>
                    <MenuItem value={3}>Manager</MenuItem>
                  </Select>
                </FormControl>
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="access_group">
                  <Typography>Access Group</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="outlined"
                  style={{ width: "100%" }}
                >
                  <Select
                    labelId="demo-customized-select-label"
                    id="demo-customized-select"
                    value={accessGroup}
                    onChange={(e) => setAccessGroup(e.target.value)}
                  >
                    <MenuItem value={0}>
                      <em>Select a group</em>
                    </MenuItem>
                    <MenuItem value={1}>Passengers</MenuItem>
                    <MenuItem value={2}>Passengers</MenuItem>
                    <MenuItem value={3}>Passengers</MenuItem>
                  </Select>
                </FormControl>
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Divider light />
            </Grid>
            <Grid item xs={12} justify="center" container>
              {userData === null ? (
                <>
                  <Box mx={2}>
                    <Button
                      color="secondary"
                      variant="contained"
                      className={clsx(classes.button, classes.addMoreBtn)}
                      onClick={handleClose}
                    >
                      Add More
                    </Button>
                  </Box>
                  <Box>
                    <Button
                      color="primary"
                      variant="contained"
                      className={clsx(classes.button, classes.sendInviteBtn)}
                      onClick={handleClose}
                    >
                      Send Invite
                    </Button>
                  </Box>
                </>
              ) : (
                <Box width="60%">
                  <Button
                    color="primary"
                    variant="contained"
                    fullWidth
                    className={clsx(classes.btn, classes.closeBtn)}
                    onClick={() => setOpen(false)}
                  >
                    Close
                  </Button>
                </Box>
              )}
            </Grid>
          </Grid>
        </form>
      </Dialog>
      <ConfirmationDialog
        buttonText="Continue"
        func={() => {
          console.log("bye bye");
        }}
        isOperationDone={isOperationDone}
        setIsOperationDone={setIsOperationDone}
        message="The invite has been sent."
      />
    </div>
  );
}
