import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DeleteOutlineOutlinedIcon from "@material-ui/icons/DeleteOutlineOutlined";

import { Box, Grid, IconButton, Typography } from "@material-ui/core";
import ClearIcon from "@material-ui/icons/Clear";
import useStyles from "./deleteactivitysuccess";
import clsx from "clsx";
export default function DeleteActivitySuccess({handleParentClose}) {
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    handleParentClose();
    setOpen(true);

  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button
        //   color="secondary"
        variant="outlined"
        className={clsx(classes.btn, classes.yesBtn)}
        onClick={handleClickOpen}
      >
        Yes
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <form className={classes.form}>
          <Grid container justify="center">
            <Grid item xs={12} container alignItems="center" justify="flex-end">
              <IconButton onClick={handleClose}>
                <ClearIcon />
              </IconButton>
            </Grid>
            <Grid item xs={10}>
              <Box my={2}>
                <Typography variant="h4" align="center">
                  {" "}
                  Delete Activity
                </Typography>
              </Box>
            </Grid>

            <Grid item xs={10}>
              <Box my={3}>
                <Typography align="center">
                  Are you sure want to delete this log?
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={12} justify="center" container>
              <Box mx={2}>
                <Button
                  //   color="secondary"
                  variant="outlined"
                  className={clsx(classes.btn, classes.yesBtn)}
                  onClick={handleClose}
                >
                  Yes
                </Button>
              </Box>
              <Box>
                <Button
                  color="primary"
                  variant="contained"
                  className={clsx(classes.btn, classes.noBtn)}
                  onClick={handleClose}
                >
                  No
                </Button>
              </Box>
            </Grid>
          </Grid>
        </form>
      </Dialog>
    </div>
  );
}
