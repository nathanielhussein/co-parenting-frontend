import {
  Box,
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  InputBase,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import useStyles from "./user_expenses_style";
import SearchIcon from "@material-ui/icons/Search";
import ViewAttachment from "components/Modals/Attachment/ViewAttachment";
import {getDateFromUnix} from "utils/functions"
function UserExpenses({ rows }) {
  const classes = useStyles();
  const [searchText, setsearchText] = useState("");


  const SingleUserRow = ({
    id,
    title,
    payment_type,
    payment_schedule,
    note,
    amount,
    date,
    attachments,
    index
  }) => {
    return (
      <TableRow className={classes.userrow}>
        <TableCell align="left" component="th" scope="row">
          {index+1}
        </TableCell>
        <TableCell align="left">
          <Typography>{title}</Typography>
        </TableCell>
        <TableCell align="left">
          <Typography>{payment_type}</Typography>
        </TableCell>
        <TableCell align="left">
          <Typography>{payment_schedule}</Typography>
        </TableCell>
        <TableCell align="left">{note}</TableCell>
        <TableCell align="left">{amount}</TableCell>
        <TableCell align="left">{getDateFromUnix(date)}</TableCell>
        <TableCell align="left">
          <ViewAttachment attachments={attachments} />
        </TableCell>
      </TableRow>
    );
  };

  const [filterdRow, setFilterdRow] = useState(rows);

  useEffect(() => {
    let searchedResult = rows.filter(
      (row) => true
  //       row.kids.name.toLowerCase().includes(searchText) ||
  //       row.expenses.name.toLowerCase().includes(searchText)
    );
    setFilterdRow(searchedResult);
  }, [searchText, rows]);

  return (
    <Grid item xs={12} spacing={2} container alignItems="center">
      <Grid item xs={10}>
        <Box
          my={2}
          p={2}
          display="flex"
          alignItems="center"
          justifyContent="space-between"
          className={classes.header_search}
        >
          <Box className={classes.search_searchField}>
            <SearchIcon />
          </Box>
          <Box mx={2} width="100%">
            <InputBase
              fullWidth
              value={searchText}
              onChange={(e) => setsearchText(e.target.value)}
              placeholder="Search..."
              inputProps={{ "aria-label": "Search" }}
            />
          </Box>
        </Box>
      </Grid>
      <Grid item xs={12}>
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead className={classes.table_header}>
              <TableRow>
                <TableCell align="left">#</TableCell>
                <TableCell align="left">TITLE</TableCell>
                <TableCell align="left">PAYMENT TYPE</TableCell>
                <TableCell align="left">PAYMENT SCHEDULE</TableCell>
                <TableCell align="left">NOTE</TableCell>
                <TableCell align="left">AMOUNT</TableCell>
                <TableCell align="left">DATE</TableCell>
                <TableCell align="left">ATTACHMENT</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {filterdRow.length > 0 ? (
                <>
                  {filterdRow.map((row, index) => (
                    <SingleUserRow key={index} {...row} index={index} />
                  ))}
                </>
              ) : (
                <TableRow>
                  <TableCell>
                    <Typography variant="h5">Not Found</Typography>
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
}

export default UserExpenses;
