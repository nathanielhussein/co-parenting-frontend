import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";

import {
  AppBar,
  Box,
  Grid,
  IconButton,
  Toolbar,
  Typography,
} from "@material-ui/core";
// import ClearIcon from "@material-ui/icons/Clear";
import useStyles from "./view_expense_style";
// import clsx from "clsx";
import ReceiptIcon from "@material-ui/icons/Receipt";
import CloseIcon from "@material-ui/icons/Close";
import UserExpenses from "./UserExpenses/UserExpenses";
import { getUserExpenses } from "api/users";
import { GlobalLoader } from "components/Loaders/Loader";

export default function ExpensesModal({ connectionId }) {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [attachmentModalOpen, setAttachmentModalOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [rows, setRows] = useState([]);
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  function createData(
    id,
    title,
    payment_type,
    payment_schedule,
    note,
    amount,
    date,
    attachments
  ) {
    return {
      id,
      title,
      payment_type,
      payment_schedule,
      note,
      amount,
      date,
      attachments,
    };
  }

  useEffect(() => {
    const fetchUserExpenses = async () => {
      try {
        setLoading(true);
        const data = await getUserExpenses(connectionId);
        if (data) {
          const result = data.map((expense, index) => {
            return createData(
              expense.id,
              expense.title || "No title",
              expense.payment_type || "Unknown",
              expense.payment_schedule || "once",
              expense.note || "No Content",
              expense.amount || "0",
              expense.createdAt || "",
              expense.attachments || []
            );
          });
          setRows(result);
        }
      } catch (error) {
        console.log("fetching data error");
      }
      setLoading(false);
    };

    return fetchUserExpenses();
  }, [connectionId]);

  return (
    <div>
      <Button
        variant="outlined"
        color="primary"
        fullWidth
        startIcon={<ReceiptIcon />}
        onClick={handleClickOpen}
      >
        Expenses
      </Button>
      <Dialog
        open={open}
        fullScreen
        maxWidth="xl"
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              Expenses
            </Typography>
          </Toolbar>
        </AppBar>
        <Grid container justify="center">
          <Grid item xs={12} md={10} xl={8}>
            <Box mt={10}>
              {!loading ? <UserExpenses rows={rows} /> : <GlobalLoader />}
            </Box>
          </Grid>
        </Grid>
      </Dialog>

      <Dialog
        open={attachmentModalOpen}
        maxWidth="xl"
        onClose={() => setAttachmentModalOpen(false)}
        aria-labelledby="form-dialog-title"
      ></Dialog>
    </div>
  );
}
