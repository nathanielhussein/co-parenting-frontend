import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  form: {
    padding: "20px",
    position: "relative",
  },

  btn: {
    textTransform: "none",
    width: "150px",
    borderRadius: "20px",
  },
  continueBtn: {
    width: "200px",
    padding: "10px 20px",
    // backgroundColor: "rgba(28, 173, 40, 0.3)",
    // color: theme.palette.primary,
  },
  noBtn: {
    // backgroundColor: "rgba(255, 0, 0, 0.2)",
  },
  successIcon: {
    height:"100px", 
    width:"100px"
  }
}));

export default useStyles;
