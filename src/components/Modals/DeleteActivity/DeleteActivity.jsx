import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DeleteOutlineOutlinedIcon from "@material-ui/icons/DeleteOutlineOutlined";
import { Box, Grid, IconButton, Typography, useMediaQuery } from "@material-ui/core";
import ClearIcon from "@material-ui/icons/Clear";

import useStyles from "./deleteactivity_style";
import clsx from "clsx";
import ConfirmationDialog from "../ConfirmationDialog/ConfirmationDialog";
import { useTheme } from "@material-ui/core/styles";

export default function DeleteActivity() {
  const classes = useStyles();
  const [isDeleteConfirmationOpen, setIsDeleteConfirmationOpen] = useState(
    false
  );
  const [isOperationDone, setIsOperationDone] = useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));

  const handleDeleteConfirmation = (action) => () => {
    setIsDeleteConfirmationOpen(!isDeleteConfirmationOpen);
    if (action === "yes") {
      setIsOperationDone(true);
    }
  };

  return (
    <div>
      <Button
        variant="outlined"
        size="small"
        className={clsx(classes.btn, classes.deleteBtn)}
        // color="error"
        startIcon={<DeleteOutlineOutlinedIcon color="error" />}
        onClick={handleDeleteConfirmation("close")}
      >
        <Typography color="error" variant="subtitle1">
          Delete
        </Typography>
      </Button>
      <Dialog
        fullScreen={fullScreen}
        open={isDeleteConfirmationOpen}
        onClose={handleDeleteConfirmation("close")}
        aria-labelledby="form-dialog-title"
      >
        <form className={classes.form}>
          <Grid container alignItems="center" justify="flex-end">
            <IconButton onClick={handleDeleteConfirmation("close")}>
              <ClearIcon />
            </IconButton>
          </Grid>
          <Grid container justify="center">
            <Box my={2}>
              <Typography variant="h4" align="center" color="primary">
                <strong>Delete Activity</strong>
              </Typography>
            </Box>
          </Grid>
          <Grid container justify="center">
            <Grid item xs={10}>
              <Box my={3}>
                <Typography align="center" color="primary">
                  Are you sure want to delete this log?
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={12} justify="center" container>
              <Box mx={2}>
                <Button
                  //   color="secondary"
                  variant="outlined"
                  className={clsx(classes.btn, classes.yesBtn)}
                  onClick={handleDeleteConfirmation("yes")}
                >
                  Yes
                </Button>
              </Box>
              <Box>
                <Button
                  color="primary"
                  variant="contained"
                  className={clsx(classes.btn, classes.noBtn)}
                  onClick={handleDeleteConfirmation("close")}
                >
                  No
                </Button>
              </Box>
            </Grid>
          </Grid>
        </form>
      </Dialog>
      <ConfirmationDialog
        buttonText="Continue"
        func={() => {
          console.log("bye bye");
        }}
        isOperationDone={isOperationDone}
        setIsOperationDone={setIsOperationDone}
        message="The activity has been deleted."
      />
    </div>
  );
}
