import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";

import {
  Box,
  Divider,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  MenuItem,
  OutlinedInput,
  Select,
  Typography,
} from "@material-ui/core";
import ClearIcon from "@material-ui/icons/Clear";
import useStyles from "./addeditteammember_style";
import clsx from "clsx";
import ConfirmationDialog from "../ConfirmationDialog/ConfirmationDialog";
import { ButtonLoader } from "../../Loaders/Loader";

import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import { useFormik } from "formik";
import * as Yup from "yup";
import { editTeamMember, getAccessGroupList, inviteNewMember } from "api/teams";
import Alert from "@material-ui/lab/Alert";
export default function AddEditTeamMember({ userData = null }) {
  console.log("user data is ", userData)
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [isOperationDone, setIsOperationDone] = useState(false);
  const [loading, setLoading] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [operationMessage, setOperationMessage] = useState({
    error: false,
    message: "",
  });
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setIsOperationDone(true);
  };
  const handleShowPassword = () => {
    setShowPassword((prev) => !prev);
  };

  const ShowMessage = ({ error, message }) => {
    return (
      <Alert
        variant="filled"
        severity={error ? "error" : "success"}
        className={`alert ${error ? "alert-danger" : "alert-success"} `}
      >
        <li className="">{message}</li>
      </Alert>
    );
  };

  const [accessGroups, setAccessGroups] = useState([]);

  function createData(id, group_name) {
    return {
      id,
      group_name: group_name.toString().toUpperCase(),
    };
  }
  useEffect(() => {
    const fetchAccessGroupList = async () => {
      try {
        const data = await getAccessGroupList();
        if (data) {
          const result = data.map((role, index) => {
            return createData(index + 1, role.roleName);
          });
          setAccessGroups(result);
        }
      } catch (error) {
        console.log("fetching data error");
      }
    };

    return fetchAccessGroupList();
  }, []);

  const schema = Yup.object().shape({
    firstName: Yup.string()
      .min(3, "Must be 3 or more charachter")
      .required("Required"),
    lastName: Yup.string()
      .min(3, "Must be 3 or more charachter")
      .required("Required"),
    email: Yup.string()
      .email("Must be a valid email")
      .required("Email is Required"),

    password: Yup.string()
      .min(8, "Must be 8 characters or more")
      .required("Required"),
  });
  const formik = useFormik({
    initialValues: {
      id: userData != null ? userData.id : null,
      firstName: userData != null ? userData.firstName : "",
      lastName: userData != null ? userData.lastName : "",
      email: userData != null ? userData.email : "",
      roleName: userData != null ? userData.roleName : "USER",
      password: userData != null ? "12345678" : "",
    },
    validationSchema: schema,
    onSubmit: async (values) => {
      try {
        setLoading(true);
        let data;
        if (userData == null) {
          data = await inviteNewMember(
            values.firstName,
            values.lastName,
            values.email,
            values.password,
            values.roleName
          );
          if (data.code) {
            setOperationMessage({
              error: false,
              message: "User is Invited Successfully",
            });
          } else {
            setOperationMessage({
              error: true,
              message: "Error when inviting a user",
            });
          }
        } else {
          data = await editTeamMember(
            values.id,
            values.firstName,
            values.lastName,
            values.email,
            values.roleName
          );
          if (data.code === 200) {
            setOperationMessage({
              error: false,
              message: "User is Edited Successfully",
            });
          } else {
            setOperationMessage({
              error: true,
              message: "Error when editing a user",
            });
          }
        }
      } catch (error) {
        setOperationMessage({ error: false, message: error.message });
      }
      setTimeout(() => {
        formik.resetForm();
        setLoading(false);
        handleClose();
      }, 2000);
    },
  });

  return (
    <div>
      <Button
        className={classes.header_actions_addBtn}
        variant={userData === null ? "contained" : "outlined"}
        color="primary"
        onClick={handleClickOpen}
      >
        {userData === null ? "Invite" : "Edit"}
      </Button>
      <Dialog
        open={open}
        onClose={() => setOpen(false)}
        aria-labelledby="form-dialog-title"
      >
        <form className={classes.form} onSubmit={formik.handleSubmit}>
          <Grid spacing={2} container>
            {operationMessage.message !== "" && (
              <Grid item xs={12}>
                <ShowMessage  {...operationMessage} />
              </Grid>
            )}
            <Grid
              item
              xs={12}
              container
              alignItems="center"
              justify="space-between"
            >
              <Grid item xs={10}>
                <Typography variant="h4" color="primary">
                  <strong>
                    {userData === null
                      ? "Invite Team Member"
                      : "Edit Team Member"}
                  </strong>
                </Typography>
                <Typography variant="subtitle1" color="primary">
                  Please provide the following information:
                </Typography>
              </Grid>

              <Grid item xs={2}>
                <Box display="flex" justifyContent="flex-end">
                  <IconButton onClick={() => setOpen(false)}>
                    <ClearIcon />
                  </IconButton>
                </Box>
              </Grid>
            </Grid>

            <Grid item xs={12}>
              <Divider light />
            </Grid>
            <Grid item xs={12}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="firstName">
                  <Typography>First Name</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="filled"
                  style={{ width: "100%" }}
                >
                  <OutlinedInput
                    id="firstName"
                    name="firstName"
                    placeholder="Enter First Name"
                    error={
                      formik.touched.firstName &&
                      Boolean(formik.errors.firstName)
                    }
                    value={formik.values.firstName}
                    onChange={formik.handleChange}
                  />
                  <FormHelperText style={{ color: "red" }}>
                    {formik.touched.firstName && formik.errors.firstName ? (
                      <React.Fragment>{formik.errors.firstName}</React.Fragment>
                    ) : null}
                  </FormHelperText>
                </FormControl>
              </Box>
            </Grid>

            <Grid item xs={12}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="lastName">
                  <Typography>Lastname</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="filled"
                  style={{ width: "100%" }}
                >
                  <OutlinedInput
                    id="lastName"
                    name="lastName"
                    placeholder="Enter Last Name"
                    error={
                      formik.touched.lastName && Boolean(formik.errors.lastName)
                    }
                    value={formik.values.lastName}
                    onChange={formik.handleChange}
                  />
                  <FormHelperText style={{ color: "red" }}>
                    {formik.touched.lastName && formik.errors.lastName ? (
                      <>{formik.errors.lastName}</>
                    ) : null}
                  </FormHelperText>
                </FormControl>
              </Box>
            </Grid>

            <Grid item xs={12}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="email">
                  <Typography>Email Address</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="filled"
                  style={{ width: "100%" }}
                >
                  <OutlinedInput
                    id="email"
                    name="email"
                    placeholder="Enter Email here"
                    error={formik.touched.email && Boolean(formik.errors.email)}
                    value={formik.values.email}
                    onChange={formik.handleChange}
                  />
                  <FormHelperText style={{ color: "red" }}>
                    {formik.touched.email && formik.errors.email ? (
                      <>{formik.errors.email}</>
                    ) : null}
                  </FormHelperText>
                </FormControl>
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="role">
                  <Typography>Role</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="outlined"
                  style={{ width: "100%" }}
                >
                  <Select
                    labelId="demo-customized-select-label"
                    id="demo-customized-select"
                    placeholder="select Group"
                    name="roleName"
                    value={formik.values.roleName}
                    onChange={formik.handleChange}
                  >
                    <MenuItem
                      selected={"ADMIN" === formik.values.roleName}
                      value={"USER"}
                    >
                      <em>Select a role</em>
                    </MenuItem>
                    {accessGroups.map((role, index) => (
                      <MenuItem
                        key={index}
                        selected={role.group_name === formik.values.roleName}
                        value={role.group_name}
                      >
                        {role.group_name}
                      </MenuItem>
                    ))}
                  </Select>
                  <FormHelperText style={{ color: "red" }}>
                    {formik.touched.roleName && formik.errors.roleName ? (
                      <>{formik.errors.roleName}</>
                    ) : null}
                  </FormHelperText>
                </FormControl>
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Box mb={1}>
                <InputLabel htmlFor="password">
                  <Typography>Password</Typography>
                </InputLabel>
              </Box>
              <FormControl variant="filled" style={{ width: "100%" }}>
                <OutlinedInput
                  disabled={userData !== null}
                  id="password"
                  name="password"
                  placeholder="Type here"
                  type={showPassword ? "text" : "password"}
                  onChange={formik.handleChange}
                  error={
                    formik.touched.password && Boolean(formik.errors.password)
                  }
                  value={formik.values.password}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        disabled={userData !== null}
                        aria-label="toggle password visibility"
                        onClick={handleShowPassword}
                      >
                        {!showPassword ? (
                          <Visibility color="primary" />
                        ) : (
                          <VisibilityOff color="primary" />
                        )}
                      </IconButton>
                    </InputAdornment>
                  }
                />
                <FormHelperText style={{ color: "red" }}>
                  {formik.touched.password && formik.errors.password ? (
                    <>{formik.errors.password}</>
                  ) : null}
                </FormHelperText>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <Divider light />
            </Grid>
            <Grid item xs={12} justify="center" container>
              {userData === null ? (
                <Box width="50%">
                  <Button
                    fullWidth
                    type="submit"
                    color="primary"
                    variant="contained"
                    className={clsx(classes.button, classes.sendInviteBtn)}
                  >
                    {loading ? (
                      <ButtonLoader color="#ffffff" size={10} />
                    ) : (
                      "Send Invite"
                    )}
                  </Button>
                </Box>
              ) : (
                <Box width="50%">
                  <Button
                    type="submit"
                    color="primary"
                    variant="contained"
                    fullWidth
                    className={clsx(classes.btn, classes.closeBtn)}
                  >
                    {loading ? (
                      <ButtonLoader color="#ffffff" size={10} />
                    ) : (
                      "Save"
                    )}
                  </Button>
                </Box>
              )}
            </Grid>
          </Grid>
        </form>
      </Dialog>
      <ConfirmationDialog
        buttonText="Continue"
        func={() => {
          console.log("bye bye");
        }}
        isOperationDone={isOperationDone}
        setIsOperationDone={setIsOperationDone}
        message="The invite has been sent."
      />
    </div>
  );
}
