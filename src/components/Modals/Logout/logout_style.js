import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  form: {
    padding: "20px",
    position: "relative",
  },

  btn: {
    textTransform: "none",
    width: "150px",
    borderRadius: "20px",
  },
  logoutBtn: {
    padding: "10px 0px",
    backgroundColor: "rgba(48, 99, 122, 0.15)",
    textAlign: "center",
    display: "flex",
    justifyContent: "center",
    textTransform: "none !important",

    "&:hover": {
      border: "1px solid #30637a",
      borderRadius: "10px",
    },
  },
}));

export default useStyles;
