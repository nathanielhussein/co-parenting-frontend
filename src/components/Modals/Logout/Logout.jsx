import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

import { Box, Grid, IconButton, MenuItem, Typography } from "@material-ui/core";
import ClearIcon from "@material-ui/icons/Clear";
import useStyles from "./logout_style";
import clsx from "clsx";
import { logout, useAuthDispatch } from "../../../context";
import { GlobalLoader } from "../../Loaders/Loader";
import { useHistory } from "react-router";
export default function Logout({ isFromNav }) {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const dispatch = useAuthDispatch();
  const history = useHistory();

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      setLoading(true);
      await logout(dispatch);
      history.replace("/auth/login");
    } catch (error) {
      console.log("there is an error on logging out");
    }
    setLoading(false);
    setOpen(false);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      {isFromNav ? (
        <MenuItem onClick={handleClickOpen}>
          <Box display="flex" alignItems="center">
            <Box mr={2}>
              <ExitToAppIcon color="primary" />
            </Box>
            <Typography color="primary">Logout</Typography>
          </Box>
        </MenuItem>
      ) : (
        <Button
          onClick={handleClickOpen}
          fullWidth
          color="primary"
          className={classes.logoutBtn}
        >
          <Typography align="center" color="primary" variant="h6">
            Log out
          </Typography>
        </Button>
      )}
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        {!loading ? (
          <form className={classes.form}>
            <Grid container justify="center">
              <Grid
                item
                xs={12}
                container
                alignItems="center"
                justify="flex-end"
              >
                <IconButton onClick={handleClose}>
                  <ClearIcon />
                </IconButton>
              </Grid>
              <Grid item xs={10}>
                <Box>
                  <Typography color="primary" variant="h3" align="center">
                    Logout
                  </Typography>
                </Box>
              </Grid>

              <Grid item xs={10}>
                <Box my={2}>
                  <Typography color="primary" align="center">
                    Are you sure you want to logout?
                  </Typography>
                </Box>
              </Grid>
              <Grid item xs={12} justify="center" container>
                <Box mx={2}>
                  <Button
                    variant="outlined"
                    className={clsx(classes.btn, classes.yesBtn)}
                    onClick={handleSubmit}
                  >
                    Yes
                  </Button>
                </Box>
                <Box>
                  <Button
                    color="primary"
                    variant="contained"
                    className={clsx(classes.btn, classes.noBtn)}
                    onClick={handleClose}
                  >
                    No
                  </Button>
                </Box>
              </Grid>
            </Grid>
          </form>
        ) : (
          <GlobalLoader />
        )}
      </Dialog>
    </div>
  );
}
