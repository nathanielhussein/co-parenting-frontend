import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";

import {
  AppBar,
  Box,
  Grid,
  IconButton,
  Toolbar,
  Typography,
} from "@material-ui/core";
// import ClearIcon from "@material-ui/icons/Clear";
import useStyles from "./view_events_modal_style";
// import clsx from "clsx";
import CloseIcon from "@material-ui/icons/Close";
import UserExpenses from "./UserEvents/UserEvents";
import { getUserEvents } from "api/users";
import EventNoteIcon from "@material-ui/icons/EventNote";
import { GlobalLoader } from "components/Loaders/Loader";

export default function EventsModal({ connectionId }) {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [attachmentModalOpen, setAttachmentModalOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [rows, setRows] = useState([]);
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  function createData(
    id,
    name,
    description,
    where,
    schedule,
    children,
    notify_coparent
  ) {
    return {
      id,
      name,
      description,
      where,
      schedule,
      children,
      notify_coparent,
    };
  }

  useEffect(() => {
    const fetchUserEvents = async () => {
      try {
        setLoading(true);
        const data = await getUserEvents(connectionId);
        if (data) {
          const result = data.map((event, index) => {
            return createData(
              event.id,
              event.name || "No title",
              event.description || "Unknown",
              event.where || "once",
              event.schedule || "No Content",
              event.children || "0",
              event.notify_coparente || "Not specified"
            );
          });
          setRows(result);
        }
      } catch (error) {
        console.log("fetching data error");
      }
      setLoading(false);
    };

    return fetchUserEvents();
  }, [connectionId]);

  return (
    <div>
      <Button
        variant="outlined"
        color="primary"
        fullWidth
        startIcon={<EventNoteIcon />}
        onClick={handleClickOpen}
      >
        Events
      </Button>
      <Dialog
        open={open}
        fullScreen
        maxWidth="xl"
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              User Events
            </Typography>
          </Toolbar>
        </AppBar>
        <Grid container justify="center">
          <Grid item xs={12} md={10} xl={8}>
            <Box mt={10}>
              {!loading ? (
                <UserExpenses rows={rows} />
              ) : (
                <GlobalLoader />
              )}
            </Box>
          </Grid>
        </Grid>
      </Dialog>

      <Dialog
        open={attachmentModalOpen}
        maxWidth="xl"
        onClose={() => setAttachmentModalOpen(false)}
        aria-labelledby="form-dialog-title"
      ></Dialog>
    </div>
  );
}
