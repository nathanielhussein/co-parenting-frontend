import {
  Box,
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  InputBase,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import useStyles from "./user_events_style";
import SearchIcon from "@material-ui/icons/Search";
function UserExpenses({ rows }) {
  const classes = useStyles();
  const [searchText, setsearchText] = useState("");

  const SingleUserRow = ({
    id,
    name,
    description,
    where,
    schedule,
    children,
    notify_coparent,
    index
  }) => {
    return (
      <TableRow className={classes.userrow}>
        <TableCell align="left" component="th" scope="row">
          {index+1}
        </TableCell>
        <TableCell align="left">
          <Typography>{name}</Typography>
        </TableCell>
        <TableCell align="left">
          <Typography>{description}</Typography>
        </TableCell>
        <TableCell align="left">
          <Typography>{where}</Typography>
        </TableCell>
        <TableCell align="left">
          <Typography>{JSON.stringify(schedule)}</Typography>
        </TableCell>
        <TableCell align="left">
          <Typography>{notify_coparent ? "Yes" : "No"}</Typography>
        </TableCell>
        {/* <TableCell align="left">{pickup_time}</TableCell> */}
        {/* <TableCell align="left">{status}</TableCell> */}
        {/* <TableCell align="left">{place}</TableCell> */}
      </TableRow>
    );
  };

  const [filterdRow, setFilterdRow] = useState(rows);

  useEffect(() => {
    let searchedResult = rows.filter(
      (row) => true
      //       row.kids.name.toLowerCase().includes(searchText) ||
      //       row.expenses.name.toLowerCase().includes(searchText)
    );
    setFilterdRow(searchedResult);
  }, [searchText, rows]);

  return (
    <Grid item xs={12} spacing={2} container alignItems="center">
      <Grid item xs={10}>
        <Box
          my={2}
          p={2}
          display="flex"
          alignItems="center"
          justifyContent="space-between"
          className={classes.header_search}
        >
          <Box className={classes.search_searchField}>
            <SearchIcon />
          </Box>
          <Box mx={2} width="100%">
            <InputBase
              fullWidth
              value={searchText}
              onChange={(e) => setsearchText(e.target.value)}
              placeholder="Search..."
              inputProps={{ "aria-label": "Search" }}
            />
          </Box>
        </Box>
      </Grid>
      <Grid item xs={12}>
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead className={classes.table_header}>
              <TableRow>
                <TableCell align="left">#</TableCell>
                <TableCell align="left">EVENT NAME</TableCell>
                <TableCell align="left">DESCRIPTION</TableCell>
                <TableCell align="left">WHERE</TableCell>
                <TableCell align="left">SCHEDULE</TableCell>
                <TableCell align="left">NOTIFY CO-PARENT</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {filterdRow.length > 0 ? (
                <>
                  {filterdRow.map((row, index) => (
                    <SingleUserRow key={index} index={index} {...row} />
                  ))}
                </>
              ) : (
                <TableRow>
                  <TableCell>
                    <Typography variant="h5">Not Found</Typography>
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
}

export default UserExpenses;
