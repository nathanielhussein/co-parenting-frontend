import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    boxShadow: "0",
  },
  userrow: {
    "&:hover": {
      cursor: "pointer",
      backgroundColor: theme.palette.action.hover,
    },
  },
  table_header: {
    backgroundColor: "#eee",
  },
  attachementBtn: {
    width: "120px",
    borderRadius: "20px",
    padding: "5px 20px",
    backgroundColor: "rgba(48, 99, 122,, 0.2)",
    color: "rgba(48, 99, 122,, 0.9)",
    fontWeight: "bold",
  },
  header: {
    marginBottom: "30px",
  },
  header_search: {
    backgroundColor: "white",
    minWidth: "85%",
    padding: "5px 20px",
    borderRadius: "20px",
  },
  search_searchField: {
    display: "flex",
  },
}));
export default useStyles;
