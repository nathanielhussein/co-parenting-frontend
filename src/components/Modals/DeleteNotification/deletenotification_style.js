import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  form: {
    padding: "20px",
    position: "relative",
  },

  btn: {
    textTransform: "none",
    width: "150px",
    borderRadius: "20px",
  },
  continueBtn: {
    width: "300px",
    padding: "10px 20px",
  },
  successIcon: {
    height: "100px",
    width: "100px",
  },
}));

export default useStyles;
