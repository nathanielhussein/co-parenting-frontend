import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import DeleteOutlineOutlinedIcon from "@material-ui/icons/DeleteOutlineOutlined";

import { Typography } from "@material-ui/core";
import useStyles from "./deletenotification_style";

import clsx from "clsx";
import ConfirmationDialog from "../ConfirmationDialog/ConfirmationDialog";
import { deleteNotifications } from "api/notifications";
export default function DeleteNotification({notification}) {
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
    const handleDelete = async () => {
      try {
        await deleteNotifications(notification.id);
      } catch (error) {
        console.log(error);
      }
    };

  return (
    <div>
      <Button
        variant="outlined"
        // color="error"
        size="small"
        className={clsx(classes.btn, classes.deleteBtn)}
        startIcon={<DeleteOutlineOutlinedIcon color="error" />}
        onClick={handleClickOpen}
      >
        <Typography color="error" variant="subtitle1">
          Delete
        </Typography>
      </Button>
      <ConfirmationDialog
        buttonText="Continue"
        func={handleDelete}
        isOperationDone={open}
        setIsOperationDone={setOpen}
        message="<strong>Notifications has been deleted</strong>"
      />
    </div>
  );
}
