import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    boxShadow: "0",
  },
  form: {
    padding: "20px",
    position: "relative",
  },
  notifyBtn: {
    borderRadius: "20px",
    padding: "10px 20px",
    minWidth: "200px",
  },
  header_actions_addBtn: {
    // minWidth: "150px",
    // minWidth: "100%",
    borderRadius: "50px",
  },
  btn: {
    textTransform: "none",
    width: "150px",
    borderRadius: "20px",
  },
  continueBtn: {
    width: "200px",
    padding: "10px 20px",
  },
  successIcon: {
    height: "100px",
    width: "100px",
  },
}));
export default useStyles;
