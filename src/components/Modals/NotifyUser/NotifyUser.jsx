import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import {
  Avatar,
  Box,
  Divider,
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  ListItem,
  ListItemAvatar,
  ListItemText,
  OutlinedInput,
  TextField,
  Typography,
} from "@material-ui/core";
import ClearIcon from "@material-ui/icons/Clear";
import useStyles from "./notifyuser_style";
import { useFormik } from "formik";
import * as Yup from "yup";

import ConfirmationDialog from "../ConfirmationDialog/ConfirmationDialog";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Alert from "@material-ui/lab/Alert";
import { ButtonLoader } from "components/Loaders/Loader";
import { sendNotification } from "api/notifications";

export default function NotifyUsers({ users }) {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [isOperationDone, setIsOperationDone] = useState(false);
  const [loading, setLoading] = useState(false);
  const [operationError, setOperationError] = useState({ message: "" });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const schema = Yup.object().shape({
    usersId: Yup.array().required("Required"),
    title: Yup.string().required("Required"),
    description: Yup.string().required("Required"),
  });
  const formik = useFormik({
    initialValues: {
      usersId: [],
      title: "",
      description: "",
    },
    validationSchema: schema,
    onSubmit: async (values) => {
      try {
        setLoading(true);
        console.log("values ", values)
        const { data, code } = await sendNotification(values);
        if (code === 200 && data) {
          console.log("notification has been sent");
          setIsOperationDone(true);
        }
      } catch (error) {
        setOperationError({
          message: "There is an error on creating notification",
        });
      }
      setLoading(false);
      setIsOperationDone(true);
      setOpen(false);
      formik.resetForm();
    },
  });

  const ShowError = ({ errorMessage }) => {
    return (
      <Alert variant="filled" severity="error" className="alert alert-danger">
        <h4 className="">{errorMessage}</h4>
      </Alert>
    );
  };

  const handleChange = (e, values) => {
    formik.setValues({
      ...formik.values,
      usersId: values.map((value) => value.id),
    });
  };

  return (
    <div>
      <Box
        width="100%"
        display="flex"
        alignItems="center"
        onClick={handleClickOpen}
      >
        <Button
          fullWidth
          className={classes.header_actions_addBtn}
          variant="contained"
          color="primary"
          onClick={handleClickOpen}
        >
          Add New
        </Button>
      </Box>
      <Dialog
        open={open}
        onClose={()=> setOpen(false)}
        aria-labelledby="form-dialog-title"
      >
        <form className={classes.form} onSubmit={formik.handleSubmit}>
          <Grid spacing={2} container>
            {operationError.message !== "" && (
              <Grid item xs={12}>
                <ShowError errorMessage={operationError.message} />
              </Grid>
            )}
            <Grid
              item
              xs={12}
              container
              alignItems="center"
              justify="space-between"
            >
              <Grid item xs={10}>
                <Typography variant="h4" color="primary">
                  <strong>Notify Users</strong>
                </Typography>
              </Grid>

              <Grid item xs={2}>
                <Box display="flex" justifyContent="flex-end">
                  <IconButton onClick={() => setOpen(false)}>
                    <ClearIcon />
                  </IconButton>
                </Box>
              </Grid>
            </Grid>

            <Grid item xs={12}>
              <Divider light />
            </Grid>
            <Grid item xs={12}>
              <InputLabel htmlFor="firstName">
                <Typography color="primary">Select Member</Typography>
              </InputLabel>
              <FormControl fullWidth>
                <Autocomplete
                  multiple
                  autoHighlight
                  id="users list"
                  name="emails"
                  limitTags={5}
                  // onChange={formik.handleChange}
                  onChange={handleChange}
                  // value={formik.values.emails}

                  options={users}
                  getOptionLabel={(users) => users.email}
                  filterSelectedOptions
                  error={formik.touched.emails && Boolean(formik.errors.emails)}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant="outlined"
                      placeholder="Select Members "
                    />
                  )}
                  renderOption={(params) => (
                    <ListItem>
                      <ListItemAvatar>
                        <Avatar src="/images/friend1.jpg" />
                      </ListItemAvatar>
                      <ListItemText>{params.email}</ListItemText>
                    </ListItem>
                  )}
                />
              </FormControl>
            </Grid>

            <Grid item xs={12}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="email">
                  <Typography>Notification Title</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="filled"
                  style={{ width: "100%" }}
                >
                  <OutlinedInput
                    id="title"
                    name="title"
                    placeholder="Enter your title"
                    value={formik.values.title}
                    onChange={formik.handleChange}
                    error={formik.touched.title && Boolean(formik.errors.title)}
                  />
                </FormControl>
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="email">
                  <Typography>Notification Description</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="filled"
                  style={{ width: "100%" }}
                >
                  <OutlinedInput
                    id="description"
                    name="description"
                    multiline
                    rows={4}
                    placeholder="Enter your description"
                    value={formik.values.description}
                    onChange={formik.handleChange}
                    error={
                      formik.touched.description &&
                      Boolean(formik.errors.description)
                    }
                  />
                </FormControl>
              </Box>
            </Grid>

            <Grid item xs={12} justify="center" container>
              <Button
                type="submit"
                color="primary"
                variant="contained"
                className={classes.notifyBtn}
              >
                {loading ? (
                  <ButtonLoader color="#ffffff" size={10} />
                ) : (
                  "Notifiy"
                )}
              </Button>
            </Grid>
          </Grid>
        </form>
      </Dialog>
      <ConfirmationDialog
        buttonText="Continue"
        func={() => {
          console.log("bye bye");
        }}
        isOperationDone={isOperationDone}
        setIsOperationDone={setIsOperationDone}
        message="The Notification has <br /> been created."
      />
    </div>
  );
}
