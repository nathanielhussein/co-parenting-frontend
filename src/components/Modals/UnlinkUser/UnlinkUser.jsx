import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";

import { Box, Grid, IconButton, Typography } from "@material-ui/core";
import ClearIcon from "@material-ui/icons/Clear";
import useStyles from "./unlinkuser_style";
import clsx from "clsx";
export default function UnlinkUser() {
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Unlink
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <form className={classes.form}>
          <Grid container alignItems="center" justify="flex-end">
            <IconButton onClick={handleClose}>
              <ClearIcon />
            </IconButton>
          </Grid>
          <Grid container justify="center">
            <Grid item xs={10}>
              <Box>
                <Typography variant="h4" color="primary" align="center">
                  <strong>Unlink User</strong>
                </Typography>
              </Box>
            </Grid>

            <Grid item xs={10}>
              <Box my={3}>
                <Typography align="center">
                  Are you sure want to unlink this user?
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={12} justify="center" container>
              <Box mx={2}>
                <Button
                  //   color="secondary"
                  variant="outlined"
                  className={clsx(classes.btn, classes.yesBtn)}
                  onClick={handleClose}
                >
                  Yes
                </Button>
              </Box>
              <Box>
                <Button
                  color="primary"
                  variant="contained"
                  className={clsx(classes.btn, classes.noBtn)}
                  onClick={handleClose}
                >
                  No
                </Button>
              </Box>
            </Grid>
          </Grid>
        </form>
      </Dialog>
    </div>
  );
}
