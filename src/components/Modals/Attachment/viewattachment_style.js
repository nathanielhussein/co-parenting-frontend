import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "20px",
    position: "relative",
  },
  btn: {
    borderRadius: "20px",
    padding: "5px 20px",
    fontWeight: "bold",
  },
  closeBtn: {
    padding: "10px 20px",
  },
  attachmentBtn: {
    width: "120px",
    backgroundColor: "rgba(48, 99, 122,, 0.2)",
    color: "rgba(48, 99, 122,, 0.9)",
  },
  attachmentImageBox: {
    height: "300px",
    width: "300px",
    overflow: "hidden",
    position: "relative",
  },
}));

export default useStyles;
