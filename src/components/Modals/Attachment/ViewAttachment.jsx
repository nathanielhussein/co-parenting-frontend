import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DescriptionOutlinedIcon from "@material-ui/icons/DescriptionOutlined";

import { Box, Grid, IconButton, Typography } from "@material-ui/core";
import ClearIcon from "@material-ui/icons/Clear";
import useStyles from "./viewattachment_style";
import clsx from "clsx";
import ArrowBackIosOutlinedIcon from "@material-ui/icons/ArrowBackIosOutlined";
import ArrowForwardIosOutlinedIcon from "@material-ui/icons/ArrowForwardIosOutlined";

export default function ViewAttachment({ attachments }) {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [currentIndex, setCurrentIndex] = useState(0);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleBack = (e) => {
    console.log("nati ", currentIndex);
    if (currentIndex > 0) {
      setCurrentIndex((currentIndex) => currentIndex - 1);
    }
  };
  const handleNext = (e) => {
    console.log("nati ", currentIndex);

    if (currentIndex < attachments.length - 1) {
      setCurrentIndex((currentIndex) => currentIndex + 1);
    }
  };


  return (
    <div className={classes.root}>
      <Button
        className={clsx(classes.btn, classes.attachmentBtn)}
        variant="outlined"
        color="primary"
        startIcon={<DescriptionOutlinedIcon color="primary" />}
        onClick={handleClickOpen}
      >
        {attachments.length} {" File"}
      </Button>

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <form className={classes.root}>
          <Grid container alignItems="center" justify="flex-end">
            <IconButton onClick={handleClose}>
              <ClearIcon />
            </IconButton>
          </Grid>
          <Grid
            container
            justify="center"
            spacing={3}
            alignItems="space-between"
          >
            <Grid
              item
              xs={12}
            >
              <Box>
                <Typography variant="h4" color="primary" align="center">
                  Attachment
                </Typography>
              </Box>
              <Box my={1}>
                <Typography align="center" color="primary">
                  File {currentIndex + 1} of {attachments.length}
                </Typography>
              </Box>
            </Grid>

            <Grid
              container
              item
              xs={12}
              spacing={2}
              className={classes.attachmentImageBox}
            >
              <Grid item xs={1} container alignItems="center" justify="center">
                <Box px={5}>
                  <IconButton
                    onClick={handleBack}
                    style={{ border: "1px solid rgba(0, 0, 0, 0.25)" }}
                  >
                    <ArrowBackIosOutlinedIcon color="primary" />
                  </IconButton>
                </Box>
              </Grid>
              <Grid
                item
                xs={10}
                container
                className={classes.attachmentImageBox}
              >
                <img
                  width="100%"
                  height="100%"
                  style={{ objectFit: "cover" }}
                  src="/images/avatar.jpg"
                  alt={"attachments[currentIndex].name"}
                />
              </Grid>
              <Grid item xs={1} container alignItems="center" justify="center">
                <IconButton
                  onClick={handleNext}
                  style={{ border: "1px solid rgba(0, 0, 0, 0.25)" }}
                >
                  <ArrowForwardIosOutlinedIcon color="primary" />
                </IconButton>
              </Grid>
            </Grid>
            <Grid item xs={12}>

            </Grid>
            <Grid
              item
              xs={6}
              // justify="center"
              // alignItems="center"
            >
              <Button
                fullWidth
                color="primary"
                variant="contained"
                className={clsx(classes.btn, classes.closeBtn)}
                onClick={handleClose}
              >
                Close
              </Button>
            </Grid>
          </Grid>
        </form>
      </Dialog>
    </div>
  );
}
