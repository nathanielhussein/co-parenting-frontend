import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";

import {
  Avatar,
  Box,
  Divider,
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  ListItem,
  ListItemAvatar,
  ListItemText,
  TextField,
  Typography,
} from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";

import useStyles from "./addaccessgroupmember_style";
import clsx from "clsx";
import ClearIcon from "@material-ui/icons/Clear";
import ConfirmationDialog from "../ConfirmationDialog/ConfirmationDialog";
export default function AddAccessGroupMember() {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [isOperationDone, setIsOperationDone] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setIsOperationDone(true);
  };

  const users = [
    {
      id: 1,
      name: "Robert Grant",
      avatar: "/images/friend1.jpg",
    },
    {
      id: 2,
      name: "Robert Grant",
      avatar: "/images/friend1.jpg",
    },
    {
      id: 3,
      name: "Robert Grant",
      avatar: "/images/friend2.jpg",
    },
    {
      id: 4,
      name: "Robert Grant",
      avatar: "/images/avatar.jpg",
    },
    {
      id: 5,
      name: "Robert Grant",
      avatar: "/images/friend1.jpg",
    },
    {
      id: 6,
      name: "Robert Grant",
      avatar: "/images/friend1.jpg",
    },
    {
      id: 1,
      name: "Robert Grant",
      avatar: "/images/friend1.jpg",
    },
    {
      id: 1,
      name: "Robert Grant",
      avatar: "/images/friend1.jpg",
    },
  ];

  return (
    <div>
      <Button
        className={classes.header_actions_addBtn}
        variant="contained"
        color="primary"
        onClick={handleClickOpen}
      >
        Add a Member
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <form className={classes.form}>
          <Grid spacing={3} container justify="center">
            <Grid
              item
              xs={12}
              container
              alignItems="center"
              justify="space-between"
            >
              <Grid item xs={10}>
                <Typography align="center" variant="h4" color="primary">
                  Add Member
                </Typography>
              </Grid>

              <Grid item xs={2}>
                <Box display="flex" justifyContent="flex-end">
                  <IconButton onClick={() => setOpen(false)}>
                    <ClearIcon />
                  </IconButton>
                </Box>
              </Grid>
            </Grid>

            <Grid item xs={12}>
              <Divider light />
            </Grid>
            <Grid item xs={10}>
              <InputLabel htmlFor="firstName">
                <Typography color="primary">Select Member</Typography>
              </InputLabel>
              <FormControl fullWidth>
                <Autocomplete
                  multiple
                  autoHighlight
                  id="users list"
                  // onChange={(e)=> console.log(e.target.)}
                  options={users}
                  getOptionLabel={(users) => users.name}
                  // defaultValue={[users[0]]}
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant="outlined"
                      placeholder="Select Members "
                    />
                  )}
                  renderOption={(params) => (
                    <ListItem>
                      <ListItemAvatar>
                        <Avatar src={params.avatar} />
                      </ListItemAvatar>
                      <ListItemText>{params.name}</ListItemText>
                    </ListItem>
                  )}
                />
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <Divider light />
            </Grid>
            <Grid item xs={12} justify="center" container>
              <Box width="60%">
                <Button
                  color="primary"
                  variant="contained"
                  fullWidth
                  className={clsx(classes.btn, classes.closeBtn)}
                  onClick={handleClose}
                >
                  Create
                </Button>
              </Box>
            </Grid>
          </Grid>
        </form>
      </Dialog>
      <ConfirmationDialog
        buttonText="Continue"
        func={() => {}}
        isOperationDone={isOperationDone}
        setIsOperationDone={setIsOperationDone}
        message="    The team Member <br /> has been added to the group."

      />
    </div>
  );
}
