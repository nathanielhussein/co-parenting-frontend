import {
  Avatar,
  Box,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import React from "react";
// import { useHistory, useRouteMatch } from "react-router";
import useStyles from "./scheduled_notificationtable_style";
import DeleteNotification from "components/Modals/DeleteNotification/DeleteNotification";

function ScheduledNotificationsTable() {
  const classes = useStyles();
  // const history = useHistory();
  // let { path, url } = useRouteMatch();

  function createData(id, id_code, title, description, scheduled_type, scheduled_time) {
    return { id, id_code, title, description, scheduled_type, scheduled_time };
  }

  const rows = [
    createData(
      "001",
      5673,
      "Notifications title ",
      "desciption here",
      "one time",
      "12:51 Am"
    ),
    createData(
      "002",
      5673,
      "Notifications title ",
      "desciption here",
      "one time",
      "12:51 Am"
    ),
    createData(
      "003",
      5673,
      "Notifications title ",
      "desciption here",
      "one time",
      "12:51 Am"
    ),
    createData(
      "004",
      5673,
      "Notifications title ",
      "desciption here",
      "one time",
      "12:51 Am"
    ),
    createData(
      "005",
      5673,
      "Notifications title ",
      "desciption here",
      "one time",
      "12:51 Am"
    ),
    createData(
      "006",
      5673,
      "Notifications title ",
      "desciption here",
      "one time",
      "12:51 Am"
    ),
    createData(
      "007",
      5673,
      "Notifications title ",
      "desciption here",
      "one time",
      "12:51 Am"
    ),
  ];

  const SingleUserRow = ({ id, id_code,title, description, scheduled_type, scheduled_time }) => {

    return (
      <TableRow className={classes.userrow} key={id}>
        <TableCell component="th" scope="row">
          {id}
        </TableCell>
        <TableCell align="left">{id_code}</TableCell>
        <TableCell align="left">
          <Box display="flex" alignItems="center">
            <Box mx={1}>
              <Avatar src="/images/friend2.jpg" />
            </Box>
            <Box mx={1}>
              <Typography>{title}</Typography>
            </Box>
          </Box>
        </TableCell>
        <TableCell align="left">{description}</TableCell>
        <TableCell align="left">{scheduled_type}</TableCell>
        <TableCell align="left">{scheduled_time}</TableCell>
        <TableCell align="center">
          <Box mx={1}>
            <DeleteNotification />
          </Box>
        </TableCell>
      </TableRow>
    );
  };

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead className={classes.table_header}>
          <TableRow>
            <TableCell>#</TableCell>
            <TableCell align="left">ID CODE</TableCell>
            <TableCell align="left">Title</TableCell>
            <TableCell align="left">DESCRIPTION</TableCell>
            <TableCell align="left">SCHEDULED TYPE</TableCell>
            <TableCell align="left">SCHEDULED TIME</TableCell>
            <TableCell align="center">ACTIONS</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <SingleUserRow {...row} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default ScheduledNotificationsTable;
