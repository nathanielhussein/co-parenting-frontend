import { Box, Grid, Tab, Tabs } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import useStyles from "./notifications_style";
import NotifyUsers from "../../Modals/NotifyUser/NotifyUser";
import NotificationsTable from "./NotificationTable/NotificationTable";
import ScheduledNotificationsTable from "./ScheduledNotificationsTable/ScheduledNotificationsTable";
import { getEveryUsers } from "api/users";

function Notications() {
  const classes = useStyles();
  const [selectedTab, setSelectedTab] = useState(0);
  //  const [searchText, setsearchText] = useState(null);
  const [allUsers, setAllUsers] = useState([]);

  const handleChange = (event, newValue) => {
    setSelectedTab(newValue);
  };

  function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && children}
      </div>
    );
  }
   useEffect(() => {
     const fetchUsersData = async () => {
       try {
         const data = await getEveryUsers();
         if (data) {
           setAllUsers(data);
         }
       } catch (error) {
         console.log("fetching data error");
       }
     };

     return fetchUsersData();
   }, []);

  return (
    <div>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        className={classes.header}
      ></Box>
      <Box my={2}>
        <Grid
          container
          style={{ borderBottom: "1px solid rgba(0, 0, 0, 0.1)" }}
          spacing={2}
          justify="space-between"
          alignItems="center"
        >
          <Grid item xs={9} md={8} className={classes.tabs}>
            <Tabs
              value={selectedTab}
              onChange={handleChange}
              aria-label="order status"
              indicatorColor="primary"
            >
              <Tab
                label="Notifications"
                id="notifications"
                aria-controls="notificatios"
              />
              <Tab
                label="Scheduled notifications"
                id="scheduled_notifications"
                aria-controls="scheduled notificatios "
              />
            </Tabs>
          </Grid>
          <Grid item xs={3} md={2}>
            <NotifyUsers users={allUsers} />
          </Grid>
          <Grid item xs={12}>
            <TabPanel value={selectedTab} index={0}>
              <NotificationsTable />
            </TabPanel>
            <TabPanel value={selectedTab} index={1}>
              <ScheduledNotificationsTable />
            </TabPanel>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
}

export default Notications;
