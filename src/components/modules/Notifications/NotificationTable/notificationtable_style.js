import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  userrow: {
    "&:hover": {
      cursor: "pointer",
      backgroundColor: theme.palette.action.hover,
    },
  },
  table_header: {
    backgroundColor: "#eee",
  },
  btn: {
    textTransform: "none",
    width: "100px",
    borderRadius: "20px",
  },
  editBtn: {
    backgroundColor: "rgba(28, 173, 40, 0.3)",
    color: theme.palette.primary,
  },
  deleteBtn: {
    backgroundColor: "rgba(255, 0, 0, 0.2)",
  },
}));
export default useStyles;
