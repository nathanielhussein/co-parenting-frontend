import {
  Avatar,
  Box,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import useStyles from "./notificationtable_style";
import DeleteNotification from "../../../Modals/DeleteNotification/DeleteNotification";
import { getAllNotifications } from "api/notifications";
import { getCreatedDate } from "utils/functions";
import { NormalLoader } from "components/Loaders/Loader";
import { Pagination } from "@material-ui/lab";

function NotificationsTable() {
  const classes = useStyles();
  const [notifications, setNotifications] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [rowPerPage] = useState(5);
  const [totalNotifications, setTotalNotifications] = useState(0);
  function createData(id, id_code, title, email, description, date) {
    return { id, id_code, title, email, description, date };
  }

  useEffect(() => {
    const fetchNotificationsData = async () => {
      setLoading(true);
      try {
        const { data, success, count } = await getAllNotifications(
          currentPage,
          rowPerPage
        );
        console.log(data, success, count, " is the notification");
        if (success) {
          const result = data.map((notification, index) => {
            return createData(
              notification.id,
              5673,
              "Robert Grant",
              "email@email.com",
              notification.description,
              getCreatedDate(notification.created_date)
            );
          });
          if (totalNotifications === 0) {
            setTotalNotifications(count);
          }
          setNotifications(result);
        }
      } catch (error) {
        console.log(error);
        console.log("fetching data error");
      }
      setLoading(false);
    };

    return fetchNotificationsData();
  }, [currentPage, rowPerPage, totalNotifications]);

  const handlePaginate = (event, value) => {
    console.log("page value is ", value);
    setCurrentPage(value);
  };

  // const handleChangeRowsPerPage = (event) => {
  //   setRowPerPage(parseInt(event.target.value, 10));
  //   setCurrentPage(1);
  // };

  const SingleNotificationRow = ({
    index,
    id,
    id_code,
    title,
    email,
    description,
    date,
  }) => {
    return (
      <TableRow className={classes.userrow}>
        <TableCell component="th" scope="row">
          {index}
        </TableCell>
        <TableCell align="left">{id_code}</TableCell>
        <TableCell align="left">
          <Box display="flex" alignItems="center">
            <Box mx={1}>
              <Avatar src="/images/friend1.jpg" />
            </Box>
            <Box mx={1}>
              <Typography>{title}</Typography>
            </Box>
          </Box>
        </TableCell>
        <TableCell align="left">{email}</TableCell>
        <TableCell align="left">{description}</TableCell>
        <TableCell align="left">{date}</TableCell>
        <TableCell align="center">
          <Box mx={1}>
            <DeleteNotification notification={{ id, title }} />
          </Box>
        </TableCell>
      </TableRow>
    );
  };

  return loading ? (
    <Box display="flex" justifyContent="center">
      <NormalLoader />
    </Box>
  ) : (
    <>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead className={classes.table_header}>
            <TableRow>
              <TableCell>#</TableCell>
              <TableCell align="left">ID CODE</TableCell>
              <TableCell align="left">TITLE</TableCell>
              <TableCell align="left">EMAIL</TableCell>
              <TableCell align="left">DESCRIPTION</TableCell>
              <TableCell align="left">DATES</TableCell>
              <TableCell align="center">Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {notifications.map((row, index) => (
              <SingleNotificationRow
                index={(currentPage - 1) * 5 + index + 1}
                key={index}
                {...row}
              />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Box my={4} display="flex" justifyContent="center">
        <Pagination
          showFirstButton
          showLastButton
          color="primary"
          size="large"
          count={parseInt(totalNotifications / rowPerPage + 1, 10)}
          page={currentPage}
          onChange={handlePaginate}
        />
      </Box>
    </>
  );
}

export default NotificationsTable;
