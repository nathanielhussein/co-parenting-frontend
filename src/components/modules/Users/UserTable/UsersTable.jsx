import {
  Avatar,
  Box,
  Button,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import clsx from "clsx";
import React from "react";
import { useHistory, useRouteMatch } from "react-router";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import useStyles from "./userstable_style";
import DeleteUser from "components/Modals/DeleteUser/DeleteUser";

function UsersTable({ rows }) {
  const classes = useStyles();
  const history = useHistory();
  let { url } = useRouteMatch();
  const SingleUserRow = ({ id, userId, username, email, date }) => {
    const handleClick = () => {
      history.push(`${url}/${userId}`);
    };

    return (
      <TableRow className={classes.userrow} key={id}>
        <TableCell component="th" scope="row">
          {id}
        </TableCell>
        <TableCell align="left">
          <Box display="flex" alignItems="center">
            <Box mx={1}>
              <Avatar src="/images/friend1.jpg" />
            </Box>
            <Box mx={1}>
              <Typography>{username}</Typography>
            </Box>
          </Box>
        </TableCell>
        <TableCell align="left">{email}</TableCell>
        <TableCell align="left">{date}</TableCell>
        <TableCell align="left">
          <Box display="flex" justifyContent="center">
            <Box mx={1}>
              <Button
                variant="outlined"
                size="small"
                color="primary"
                className={clsx(classes.btn, classes.editBtn)}
                startIcon={<EditOutlinedIcon color="primary" />}
                onClick={(e) => handleClick()}
              >
                <Typography color="primary" variant="subtitle1">
                  Edit
                </Typography>
              </Button>
            </Box>
            <Box mx={1}>
              <DeleteUser id={id} userEmail={email} />
            </Box>
          </Box>
        </TableCell>
      </TableRow>
    );
  };

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead className={classes.table_header}>
          <TableRow>
            <TableCell>#</TableCell>
            <TableCell align="left">USER NAME</TableCell>
            <TableCell align="left">EMAIL</TableCell>
            <TableCell align="left">DATE</TableCell>
            <TableCell align="center">Actions</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.length > 0 ? (
            <>
              {rows.map((row, index) => (
                <SingleUserRow key={index} {...row} />
              ))}
            </>
          ) : (
            <TableRow>
              <TableCell>
                <Typography variant="h5">Not Found</Typography>
              </TableCell>
            </TableRow>
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default UsersTable;
