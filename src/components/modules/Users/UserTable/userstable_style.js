import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  userrow: {
    "&:hover": {
      cursor: "pointer",
      backgroundColor: theme.palette.action.hover,
    },
  },
  table_header: {
    backgroundColor: "#eee",
  },
  btn: {
    textTransform: "none",
    width: "100px",
    borderRadius: "20px",
  },
  editBtn: {
    color: theme.palette.primary,
    textTransform: "none",
    width: "150px",
    borderRadius: "20px",
  },
  deleteBtn: {
    backgroundColor: "rgba(255, 0, 0, 0.2)",
  },
}));
export default useStyles;
