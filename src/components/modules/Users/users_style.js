import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    boxShadow: "0",
  },
  header: {
    marginBottom: "30px",
  },
  header_search: {
    backgroundColor: "white",
    flex: "1",
    padding: "5px 20px",
    borderRadius: "20px",
  },
  search_searchField: {
    display: "flex",
  },
  header_actions: {
    textTransform: "none",
    minWidth: "10%",
  },
  header_actions_addBtn: {
    borderRadius: "50px",
  },
  userlist: {
    minHeight: "50%"
  },
}));
export default useStyles;
