import { Box, InputBase } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import SearchIcon from "@material-ui/icons/Search";
import useStyles from "./users_style";
import UsersTable from "./UserTable/UsersTable";
import { NormalLoader } from "components/Loaders/Loader";
import { getCreatedDate } from "utils/functions";
import { getRoleTeamMembers } from "api/teams";

function Users() {
  const classes = useStyles();
  const [searchText, setsearchText] = useState("");
  const [loading, setLoading] = useState(false);
  const [rows, setRows] = useState([]);
  const [filterdRow, setFilterdRow] = useState([]);

  function createData(id, userId, username, email, date) {
    return { id, userId, username, email, date };
  }

  useEffect(() => {
    const fetchUsersData = async () => {
      try {
        setLoading(true);
        const data = await getRoleTeamMembers("USER");
        console.log(data, " is the data");
        if (data) {
          const result = data.map((user, index) => {
            return createData(
              index + 1,
              user.id,
              `${user.firstName} ${user.lastName}`,
              user.email,
              getCreatedDate(user.created_date)
            );
          });
          setRows(result);
          setFilterdRow(result);
        }
      } catch (error) {
        console.log("fetching data error", error);
      }
      setLoading(false);
    };

    return fetchUsersData();
  }, []);

  useEffect(() => {
    let searchedResult = rows.filter(
      (row) =>
        row.username.toLowerCase().includes(searchText) ||
        row.email.toLowerCase().includes(searchText)
    );
    setFilterdRow(searchedResult);
  }, [searchText, rows]);

  return (
    <div>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        className={classes.header}
      >
        <Box
          display="flex"
          alignItems="center"
          className={classes.header_search}
        >
          <Box className={classes.search_searchField}>
            <SearchIcon color="primary" />
          </Box>
          <Box mx={2} width="100%">
            <InputBase
              fullWidth
              value={searchText}
              onChange={(e) => setsearchText(e.target.value)}
              placeholder="Search..."
              color="primary"
              inputProps={{ "aria-label": "Search" }}
            />
          </Box>
        </Box>
        {/* <Box display="flex" mx={2} className={classes.header_actions}>
          <AddEditTeamMember />
        </Box> */}
      </Box>
      <Box
        className={classes.userlist}
        display="flex"
        alignItems="center"
        justifyContent="center"
      >
        {!loading ? (
          <UsersTable rows={filterdRow} />
        ) : (
          <Box>
            <NormalLoader />
          </Box>
        )}
      </Box>
    </div>
  );
}

export default Users;
