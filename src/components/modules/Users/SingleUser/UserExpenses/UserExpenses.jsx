import {
  Avatar,
  Box,
  FormControl,
  Grid,
  IconButton,
  MenuItem,
  Paper,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  InputBase,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import useStyles from "./user_expenses_style";
import SearchIcon from "@material-ui/icons/Search";
import ViewAttachment from "../../../../Modals/Attachment/ViewAttachment";

function UserExpenses({ rows }) {
  const classes = useStyles();
  const [selectedStatus, setSelectedStatus] = useState(0);
  const [searchText, setsearchText] = useState("");

  const SingleUserRow = ({
    id,
    kids,
    expenses,
    payment_schedule,
    amount,
    status,
    time,
    date,
    attachments,
  }) => {
    return (
      <TableRow className={classes.userrow} key={id}>
        <TableCell component="th" scope="row">
          {id}
        </TableCell>
        <TableCell align="left">
          <Box display="flex" alignItems="center">
            <IconButton>
              <Avatar src={kids.avatar} className={classes.avatar} />
            </IconButton>
            <Typography>{kids.name}</Typography>
          </Box>
        </TableCell>
        <TableCell align="left">
          <Box display="flex" alignItems="center">
            <Typography>{expenses.name}</Typography>
          </Box>
        </TableCell>
        <TableCell align="left">
          <Typography>{payment_schedule}</Typography>
        </TableCell>
        <TableCell align="left">{amount}</TableCell>
        <TableCell align="left">
          <Typography variant="subtitle1" color="primary">
            {status}
          </Typography>
        </TableCell>
        <TableCell align="left">{time}</TableCell>
        <TableCell align="left">{date}</TableCell>
        <TableCell align="left">
          <ViewAttachment attachments={attachments} />
        </TableCell>
      </TableRow>
    );
  };

  const [filterdRow, setFilterdRow] = useState(rows);

  useEffect(() => {
    let searchedResult = rows.filter(
      (row) =>
        row.kids.name.toLowerCase().includes(searchText) ||
        row.expenses.name.toLowerCase().includes(searchText)
    );
    setFilterdRow(searchedResult);
  }, [searchText, rows]);

  return (
    <Grid item xs={12} spacing={2} container alignItems="center">
      <Grid item xs={10}>
        <Box
          my={2}
          p={2}
          display="flex"
          alignItems="center"
          justifyContent="space-between"
          className={classes.header_search}
        >
          <Box className={classes.search_searchField}>
            <SearchIcon />
          </Box>
          <Box mx={2} width="100%">
            <InputBase
              fullWidth
              value={searchText}
              onChange={(e) => setsearchText(e.target.value)}
              placeholder="Search..."
              inputProps={{ "aria-label": "Search" }}
            />
          </Box>
        </Box>
      </Grid>
      <Grid item xs={2}>
        <FormControl
          variant="outlined"
          className={classes.formControl}
          fullWidth
          size="small"
        >
          <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={selectedStatus}
            onChange={(e) => setSelectedStatus(e.target.value)}
          >
            <MenuItem value={0}>Status:All</MenuItem>
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
          </Select>
        </FormControl>{" "}
      </Grid>
      <Grid item xs={12}>
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead className={classes.table_header}>
              <TableRow>
                <TableCell>#</TableCell>
                <TableCell align="left">KIDS</TableCell>
                <TableCell align="left">EXPENSES</TableCell>
                <TableCell align="left">PAYMENT SCHEDULE</TableCell>
                <TableCell align="left">AMOUNT</TableCell>
                <TableCell align="left">STATUS</TableCell>
                <TableCell align="left">TIME</TableCell>
                <TableCell align="left">Date</TableCell>
                <TableCell align="left">ATTACHMENT</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {filterdRow.length > 0 ? (
                <>
                  {filterdRow.map((row, index) => (
                    <SingleUserRow key={index} {...row} />
                  ))}
                </>
              ) : (
                <TableRow>
                  <TableCell>
                    <Typography variant="h5">Not Found</Typography>
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
}

export default UserExpenses;
