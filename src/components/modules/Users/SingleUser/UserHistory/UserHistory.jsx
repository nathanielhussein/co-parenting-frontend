import {
  Avatar,
  Box,
  FormControl,
  Grid,
  IconButton,
  MenuItem,
  Paper,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  InputBase,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import useStyles from "./userhistory_style";
import SearchIcon from "@material-ui/icons/Search";
import DeleteActivity from "../../../../Modals/DeleteActivity/DeleteActivity";

function UserHistory({ rows }) {
  const classes = useStyles();
  const [selectedStatus, setSelectedStatus] = useState(0);
  const [searchText, setsearchText] = useState("");

  const [filterdRow, setFilterdRow] = useState(rows);

  useEffect(() => {
    let searchedResult = rows.filter(
      (row) =>
        row.co_parent.toLowerCase().includes(searchText) ||
        row.kids[0].name.toLowerCase().includes(searchText) ||
        row.kids[1].name.toLowerCase().includes(searchText)
    );
    setFilterdRow(searchedResult);
  }, [searchText, rows]);

  const SingleUserRow = ({
    id,
    co_parent,
    kids,
    event,
    detail,
    time,
    date,
  }) => {
    return (
      <TableRow className={classes.userrow} key={id}>
        <TableCell component="th" scope="row">
          {id}
        </TableCell>
        <TableCell align="left">
          <Box display="flex" alignItems="center">
            <IconButton>
              <Avatar src="/images/avatar.jpeg" className={classes.avatar} />
            </IconButton>
            <Typography>{co_parent}</Typography>
          </Box>
        </TableCell>
        <TableCell align="left">
          <Typography>View Children</Typography>

          {/* {kids.map((kid, index) => (
            <Box key={index} display="flex" alignItems="center" >
              <IconButton>
                <Avatar
                  src="/images/friend1.jpg"
                  className={classes.avatar}
                />
              </IconButton>
              <Typography>{kid.name}</Typography>
            </Box>
          ))} */}
        </TableCell>
        <TableCell align="left">
          <Typography>View</Typography>
        </TableCell>

        <TableCell align="left">
          {/* <Typography>{event}</Typography> */}
          <Typography>View</Typography>
        </TableCell>
        <TableCell align="left">
          <Box height={24} width={24} borderRadius="50%" bgcolor="primary">
            {detail}
          </Box>
        </TableCell>
        <TableCell align="left">{time}</TableCell>
        <TableCell align="left">{date}</TableCell>
        <TableCell align="center">
          <DeleteActivity />
        </TableCell>
      </TableRow>
    );
  };

  return (
    <Grid item xs={12} spacing={2} container alignItems="center">
      <Grid item xs={10}>
        <Box
          my={2}
          p={2}
          display="flex"
          alignItems="center"
          justifyContent="space-between"
          className={classes.header_search}
        >
          <Box className={classes.search_searchField}>
            <SearchIcon />
          </Box>
          <Box mx={2} width="100%">
            <InputBase
              fullWidth
              value={searchText}
              onChange={(e) => setsearchText(e.target.value)}
              placeholder="Search..."
              inputProps={{ "aria-label": "Search" }}
            />
          </Box>
        </Box>
      </Grid>
      <Grid item xs={2}>
        <FormControl
          variant="outlined"
          className={classes.formControl}
          fullWidth
          size="small"
        >
          <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={selectedStatus}
            onChange={(e) => setSelectedStatus(e.target.value)}
          >
            <MenuItem value={0}>Status:All</MenuItem>
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
          </Select>
        </FormControl>{" "}
      </Grid>

      <Grid item xs={12}>
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead className={classes.table_header}>
              <TableRow>
                <TableCell>#</TableCell>
                <TableCell align="left">CO PARENT</TableCell>
                <TableCell align="left">KIDS</TableCell>
                <TableCell align="left">Expenses</TableCell>
                <TableCell align="left">EVENT</TableCell>
                <TableCell align="left">DETAIL</TableCell>
                <TableCell align="left">TIME</TableCell>
                <TableCell align="left">Date</TableCell>
                <TableCell align="center">ACTION</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {filterdRow.length > 0 ? (
                <React.Fragment>
                  {filterdRow.map((row, index) => (
                    <SingleUserRow key={index} {...row} />
                  ))}
                </React.Fragment>
              ) : (
                <TableRow>
                  <TableCell>
                    <Typography variant="h5">Not Found</Typography>
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
}

export default UserHistory;
