import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    boxShadow: "0",
  },
  avatar: {
    width: theme.spacing(4.5),
    height: theme.spacing(4.5),
  },
  userrow: {
    "&:hover": {
      cursor: "pointer",
      backgroundColor: theme.palette.action.hover,
    },
  },
  table: {
    // border: "1px solid red",
  },
  table_header: {
    backgroundColor: "#eee",
  },
  header: {
    marginBottom: "30px",
  },
  header_search: {
    backgroundColor: "white",
    minWidth: "85%",
    padding: "5px 20px",
    borderRadius: "20px",
  },
  search_searchField: {
    display: "flex",
  },
}));
export default useStyles;
