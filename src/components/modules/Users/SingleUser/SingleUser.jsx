import {
  Box,
  Divider,
  Grid,
  IconButton,
  Paper,
  Tab,
  Tabs,
  Typography,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import PrintOutlinedIcon from "@material-ui/icons/PrintOutlined";
import useStyles from "./singleuser_style";
import UserDetail from "./UserDetail/UserDetail";
import UserHistory from "./UserHistory/UserHistory";
import EmailData from "../../../Modals/EmailData/EmailData";
import { getUserDetails } from "api/users";
import { GlobalLoader } from "components/Loaders/Loader";
function SingleUser({ id }) {
  const classes = useStyles();
  const [selectedTab, setSelectedTab] = React.useState(0);
  const [userDetail, setUserDetail] = useState({});
  const [loading, setLoading] = useState(false);
  const handleChange = (event, newValue) => {
    setSelectedTab(newValue);
  };

  useEffect(() => {
    const fetchUserDetails = async () => {
      try {
        setLoading(true);
        const data = await getUserDetails(id);
        if (data) {
          setUserDetail(data);
          console.log("results are ", data);
        }
      } catch (error) {
        console.log("fetching data error");
      }
      setLoading(false);
    };
    return fetchUserDetails();

  }, [id]);

  function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && children}
      </div>
    );
  }

  function createUserHistoryData(
    id,
    co_parent,
    kids,
    event,
    detail,
    time,
    date
  ) {
    return { id, co_parent, kids, event, detail, time, date };
  }

  const userHistoryDataList = [
    createUserHistoryData(
      "001",
      "John Doe",
      [{ name: "samuel" }, { name: "thomas 2" }],
      "Added new Event",
      "Accepted",
      "12:22 am",
      "12.22.22"
    ),
    createUserHistoryData(
      "002",
      "Jenna Chen",
      [{ name: "george " }, { name: "nathan" }],
      "Added new Event",
      "Accepted",
      "12:22 am",
      "12.22.22"
    ),
    createUserHistoryData(
      "003",
      "Jane Doe ",
      [{ name: "Kid 2" }, { name: "Kid 2" }],
      "Added new Event",
      "Accepted",
      "12:22 am",
      "12.22.22"
    ),
    createUserHistoryData(
      "004",
      "Jenna Chen",
      [{ name: "Kid 2" }, { name: "Kid 2" }],
      "Added new Event",
      "Accepted",
      "12:22 am",
      "12.22.22"
    ),
    createUserHistoryData(
      "005",
      "Jenna Chen",
      [{ name: "Kid 2" }, { name: "Kid 2" }],
      "Added new Event",
      "Accepted",
      "12:22 am",
      "12.22.22"
    ),
  ];

  return (
    <div>
      {loading ? (
        <GlobalLoader />
      ) : (
      <Grid container style={{ borderBottom: "1px solid rgba(0, 0, 0, 0.1)" }}>
        <Grid item md={6} xl={4} sm={8} xs={12} className={classes.tabs}>
          <Paper>
            <Box
              p={1}
              display="flex"
              justifyContent="space-between"
              alignItems="center"
            >
              <Box display="flex">
                <Typography align="left" variant="h6">
                  Viewing:{" "}
                </Typography>
                <Box mx={1}>
                  <Typography
                    align="right"
                    style={{ fontWeight: "bold" }}
                    variant="h6"
                  >
                    {userDetail.userEmail}
                  </Typography>
                </Box>
              </Box>

              <Box display="flex">
                <EmailData />
                <Divider orientation="vertical" />
                <IconButton>
                  <PrintOutlinedIcon />
                </IconButton>
              </Box>
            </Box>
            <Box>
              <Divider />
            </Box>
            <Box my={1}>
              <Tabs
                value={selectedTab}
                onChange={handleChange}
                aria-label="order status"
                indicatorColor="primary"
              >
                <Tab label="Detail" id="detail" aria-controls="detauk" />
                <Tab label="History" id="history" aria-controls="history" />
                {/* <Tab label="Expenses" id="history" aria-controls="history" /> */}
              </Tabs>
            </Box>
          </Paper>
        
        </Grid>
        <Grid item xs={12}>
          <TabPanel value={selectedTab} index={0}>
            <UserDetail userDetail={userDetail} />
          </TabPanel>
          <TabPanel value={selectedTab} index={1}>
            <UserHistory rows={userHistoryDataList} />
          </TabPanel>
          {/* <TabPanel value={selectedTab} index={2}> */}
          {/* <UserExpenses rows={UserExpensesList} /> */}
          {/* </TabPanel> */}
        </Grid>
      </Grid>
      )}
    </div>
  );
}

export default SingleUser;
