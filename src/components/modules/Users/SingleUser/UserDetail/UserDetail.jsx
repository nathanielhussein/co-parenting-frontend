import {
  Avatar,
  Box,
  Divider,
  Grid,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Paper,
  Typography,
} from "@material-ui/core";
import React from "react";
import AddEditUser from "../../../../Modals/AddEditUser/AddEditUser";
import useStyles from "./userdetail_style";
import ExpensesModal from "components/Modals/ViewExpenseTable/ViewExpenses";
import EventsModal from "components/Modals/ViewEventsTable/ViewEventsModal";
function UserDetail({ userDetail }) {
  const classes = useStyles();
  console.log(" user detail is ", userDetail);

  return (
    <div>
      <Grid
        className={classes.root}
        item
        xs={12}
        md={12}
        lg={10}
        container
        spacing={3}
        alignItems="stretch"
      >
        <Grid item xs={12} md={6}>
          <Paper className={classes.paper}>
            <Box
              display="flex"
              alignItems="center"
              justifyContent="space-between"
            >
              <Typography>User Details</Typography>
              <AddEditUser userData={{ username: "something" }} />
            </Box>
            <Box display="flex" alignItems="center">
              <Box mr={3}>
                <Avatar src="/images/avatar.jpeg" className={classes.avatar}>
                  {userDetail.userEmail && userDetail.userEmail.charAt(0)}
                </Avatar>
              </Box>
              <Box>
                <Box my={2}>
                  <Typography variant="h6">{userDetail.username}</Typography>
                  <Typography variant="body1" color="textSecondary">
                    {userDetail.userEmail}
                  </Typography>
                </Box>
                <Box my={2}>
                  <Typography color="textSecondary">ID Code</Typography>
                  <Typography>01125</Typography>
                </Box>
              </Box>
            </Box>
          </Paper>
        </Grid>
        <Grid item xs={12}></Grid>
        <Grid item xs={12} container spacing={2}>
          {userDetail.coparents != null &&
            userDetail.coparents.map((parentData, index) => {
              return (
                <Grid
                  key={index}
                  item
                  xs={12}
                  md={6}
                  xl={4}
                  className={classes.card}
                >
                  <Paper className={classes.paper}>
                    <Grid item xs>
                      <Typography>Co parenting with</Typography>
                      <Box mt={1} display="flex" alignItems="center">
                        <Box mr={3}>
                          <Avatar
                            src="/images/friend1.jpg"
                            className={classes.avatar}
                          >
                            N
                          </Avatar>
                        </Box>
                        <Box>
                          <Box my={2}>
                            <Typography variant="h6">
                              {parentData.name || "No Name"}
                            </Typography>
                            <Typography variant="body1" color="textSecondary">
                              {parentData.email}
                            </Typography>
                          </Box>
                        </Box>
                      </Box>
                      <Divider />
                      <Box>
                        <Box my={1}>
                          <Typography variant="h6" color="textSecondary">
                            Kids
                          </Typography>
                        </Box>
                        <List>
                          {parentData.children.length > 0 ? (
                            parentData.children.map((child, index) => {
                              const fullName = `${child.firstName} ${child.lastName}`;
                              return (
                                <ListItem key={index}>
                                  <ListItemAvatar>
                                    <Avatar src="/images/friend2.jpg">K</Avatar>
                                  </ListItemAvatar>
                                  <ListItemText
                                    primary={fullName.trim() || "No Name"}
                                  ></ListItemText>
                                </ListItem>
                              );
                            })
                          ) : (
                            <>
                              <ListItem>
                                <ListItemText primary="No Kids"></ListItemText>
                              </ListItem>
                              <ListItem>
                                <ListItemText primary="">
                                  &nbsp; &nbsp;
                                </ListItemText>
                              </ListItem>
                            </>
                          )}
                        </List>
                      </Box>
                    </Grid>
                    <Grid
                      item
                      xs
                      container
                      alignItems="flex-end"
                      spacing={2}
                      justify="center"
                    >
                      <Grid item xs={6}>
                        <ExpensesModal connectionId={parentData.connectionId} />
                      </Grid>
                      <Grid item xs={6}>
                        <EventsModal connectionId={parentData.connectionId}/>
                      </Grid>
                    </Grid>
                  </Paper>
                </Grid>
              );
            })}
        </Grid>
      </Grid>
    </div>
  );
}

export default UserDetail;
