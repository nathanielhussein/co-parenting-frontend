import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    boxShadow: "0",
    position: "relative", 
    height: "100%"
  },
  paper: {
    padding: "20px",
    height: "100%", 
    display: "flex", 
    flexDirection: "column", 
    justifyContent: "space-between"
  },
  card: {
    // display:"flex", 
    width: "100%",
    alignItems: "stretch"
    // margin: "20px"
  },
  avatar: {
      width: theme.spacing(12),
      height: theme.spacing(12),
  },
}));
export default useStyles;
