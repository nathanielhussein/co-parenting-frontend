import {
  Box,
  FormControl,
  Grid,
  MenuItem,
  Paper,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import useStyles from "./manageusers_style";

function ManageUsers() {
  const classes = useStyles();
  function createData(id, features, places, status, date) {
    return { id, features, places, status, date };
  }
  const placesList = [
    {
      name: "Everywhere",
      value: "everywhere",
    },
    {
      name: "Specific",
      value: "specific",
    },
  ];

    const statusesList = [
      {
        name: "Available",
        value: "available",
      },
      {
        name: "2 day /week",
        value: "twoperweek",
      },
    ];

  const rows = [
    createData(
      1,
      "Scheduling",
      "everywhere",
      "available",
    ),
    createData(
      2,
      "Swap Request",
      "everywhere",
      "available",
    ),
    createData(
      3,
      "Money Transfer",
      "everywhere",
      "available",
    ),
    createData(
      4,
      "Expense setup",
      "everywhere",
      "available",
    ),
    createData(
      5,
      "Messageing",
      "everywhere",
      "available",
    ),
  ];

  const SingleRow = ({ features, places, status }) => {
    return (
      <TableRow className={classes.userrow} key={features}>
        <TableCell align="left">{features}</TableCell>
        <TableCell align="left">
          <FormControl
            size="small"
            variant="outlined"
            style={{ minWidth: "50%" }}
          >
            <Select
              labelId="demo-customized-select-label"
              id="demo-customized-select"
              // value={accessGroup}
              // onChange={(e) => setAccessGroup(e.target.value)}
            >
              {placesList.map((place, index) => {
                return <MenuItem selected={true} value={index}>{place.name}</MenuItem>;
              })}
            </Select>
          </FormControl>
        </TableCell>
        <TableCell align="left">
          <FormControl
            size="small"
            variant="outlined"
            style={{ width: "60%" }}
          >
            <Select
              labelId="demo-customized-select-label"
              id="demo-customized-select"
              // value={accessGroup}
              // onChange={(e) => setAccessGroup(e.target.value)}
            >
              {statusesList.map((st, index) => {
                return <MenuItem selected={true} value={index}>{st.name}</MenuItem>;
              })}
            </Select>
          </FormControl>
        </TableCell>
      </TableRow>
    );
  };
  return (
    <Grid item xs={8}>
      <Paper>
        <Box
          my={2}
          p={2}
          display="flex"
          alignItems="center"
          justifyContent="space-between"
        >
          <Box flexGrow={8}>
            <Typography>Settings </Typography>
          </Box>
        </Box>
      </Paper>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead className={classes.table_header}>
            <TableRow>
              <TableCell align="left">FEATURES</TableCell>
              <TableCell align="left">PLACES</TableCell>
              <TableCell align="left">STATUS</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <SingleRow {...row} />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Grid>
  );
}

export default ManageUsers;

