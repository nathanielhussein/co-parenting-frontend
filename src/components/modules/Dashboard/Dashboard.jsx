import { Box, Divider, Grid, Paper, Typography } from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import React from "react";
import useStyles from "./dashboard_style";
import MapChart from "./OrganicTraffic/MapChart/MapChart";
import TopTrafficStates from "./TopTrafficStates/TopTrafficStates";
import TrafficSource from "./TrafficSource/TrafficSource";
import UsersChart from "./UsersChart/UsersChart";
function Dashboard() {
  const classes = useStyles();
  return (
    <>
      {/* <Paper className={classes.root}> */}
      <Grid container spacing={3}>
        <Grid item xs={12} className={classes.users_chart}>
          <Paper className={classes.paper}>
            <Box color="secondary" className={classes.users_chart_header}>
              <Typography variant="h5">Users</Typography>
              <Box display="flex" justifyContent="center" alignItems="center">
                <Box mx={1}></Box>
                <Box mx={1}>
                  <Typography color="textSecondary">Today</Typography>
                </Box>

                <Box mx={1}>
                  <Typography>March 11, 2020</Typography>
                </Box>
                <Divider orientation="vertical" />
                <Box mx={1} color="text.secondary">
                  <Typography>Wednesday</Typography>
                </Box>
                <Divider orientation="vertical" />

                <Box mx={1}>
                  <Typography>Week 9</Typography>
                </Box>
                <ExpandMoreIcon />
              </Box>
              <Box display="flex" justifyContent="center">
                <Box mx={1}>
                  <Typography>Week 9</Typography>
                </Box>
                <ExpandMoreIcon />
              </Box>
            </Box>
            <Divider />
            <Box m={1} py={2} >
              <UsersChart />
            </Box>
          </Paper>
        </Grid>
        <Grid item xs={12} md={7}>
          <Paper className={classes.paper}>
            <Box color="secondary" className={classes.users_chart_header}>
              <Typography variant="h5">Organic Traffic In USA</Typography>
            </Box>
            <Divider />
            <Grid item container justify="center" alignItems="center" xs={12}>
              <Grid item xs={8}>
                <Box px={1} display="flex" justifyContent="center">
                  <MapChart />
                </Box>
              </Grid>
              <Grid item xs={12} md={4}>
                <Box px={2}>
                  <TopTrafficStates />
                </Box>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
        <Grid item xs={12} md={5}>
          <Paper className={classes.paper}>
            <Box color="secondary" className={classes.users_chart_header}>
              <Typography variant="h5">Traffic Sources</Typography>
            </Box>
            <Divider />
            <Box
              display="flex"
              alignItems="center"
              px={3}
              className={classes.trafficSource}
            >
              <TrafficSource />
            </Box>
          </Paper>
        </Grid>
      </Grid>
      {/* </Paper> */}
    </>
  );
}

export default Dashboard;
