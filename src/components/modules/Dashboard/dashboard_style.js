import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "80vh",
  },
  users_chart: {
    //   padding: "10px 20px"
  },
  users_chart_header: {
    display: "flex",
    justifyContent: "space-between",
    padding: "15px 30px",
    backgroundColor: "rgba(150, 150, 150, 0.1)",
  },
  paper: {
    minHeight: "100%",
    display: "flex", 
    flexDirection: "column",
  },
  trafficSource: {
    flex: 1,
    position: "relative"
  },
}));
export default useStyles;
