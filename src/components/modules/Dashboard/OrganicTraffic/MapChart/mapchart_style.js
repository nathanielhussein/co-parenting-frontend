import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "400px",
    width: "500px",
    position: "relative",
    textAlign: "center"
  },
  zoomBtnsContainer:{
    position: "absolute", 
    top: "10px", 
    right: "10px",
    display: "flex", 
    flexDirection: "column"
  },
  zoomBtns: {
    border: "1px solid rgba(0, 0, 0, 0.5)"
  }
}));
export default useStyles;
