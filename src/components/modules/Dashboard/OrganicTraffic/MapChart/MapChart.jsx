import { Box, IconButton } from "@material-ui/core";
import React, { memo, useState } from "react";
import {
  ComposableMap,
  Geographies,
  Geography,
  ZoomableGroup,
} from "react-simple-maps";
import RemoveIcon from "@material-ui/icons/Remove";
import AddIcon from "@material-ui/icons/Add";
import useStyles from "./mapchart_style";
const geoUrl = "https://cdn.jsdelivr.net/npm/us-atlas@3/states-10m.json";

const MapChart = () => {
  const classes = useStyles();
  const [position, setPosition] = useState({ coordinates: [0, 0], zoom: 1 });

  function handleZoomIn() {
    if (position != null) {
      if (position.zoom >= 4) return;
      setPosition((pos) => ({ ...pos, zoom: pos.zoom * 2 }));
    }
  }

  function handleZoomOut() {
    if (position != null) {
      if (position.zoom <= 1) return;
      setPosition((pos) => ({ ...pos, zoom: pos.zoom / 2 }));
    }
  }

  // function handleMoveEnd(position) {
  //   setPosition(position);
  // }

  return (
    <div className={classes.root}>
      <ComposableMap projection="geoAlbersUsa">
        <ZoomableGroup
          // zoomAndPan
          // zoom={position != null ? position.zoom : 1}
          // center={position != null ? position.coordinates : [0, 0]}
          // onMoveEnd={handleMoveEnd}
          // onMoveStart={handleMoveEnd}
        >
          <Geographies geography={geoUrl}>
            {({ geographies }) => (
              <>
                {geographies.map((geo, index) => (
                  <Geography
                    key={geo.rsmKey}
                    stroke="#FFF"
                    geography={geo}
                    fill={index % 7 === 0 ? "#30637a" : "#DDD"}
                    style={{
                      hover: {
                        fill: "#3063fa",
                        outline: "none",
                      },
                    }}
                    onMouseEnter={() => {
                      // const { NAME, POP_EST } = geo.properties;
                      // console.log(geo.properties);
                      // setTooltipContent(`${NAME} — ${rounded(POP_EST)}`);
                    }}
                    onMouseLeave={() => {
                      // setTooltipContent("");
                    }}
                  />
                ))}
              </>
            )}
          </Geographies>
        </ZoomableGroup>
      </ComposableMap>
      <Box mb={1} display="flex" className={classes.zoomBtnsContainer}>
        <Box my={1}>
          <IconButton onClick={handleZoomIn} className={classes.zoomBtns}>
            <AddIcon color="primary" />
          </IconButton>
        </Box>
        <Box my={1}>
          <IconButton onClick={handleZoomOut} className={classes.zoomBtns}>
            <RemoveIcon color="primary" />
          </IconButton>
        </Box>
      </Box>
    </div>
  );
};

export default memo(MapChart);
