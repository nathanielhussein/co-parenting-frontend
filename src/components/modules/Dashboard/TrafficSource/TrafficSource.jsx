import React from "react";
import LinearProgress from "@material-ui/core/LinearProgress";
import { Box, Grid, Typography } from "@material-ui/core";
import useStyles from "./trafficsource_style";

function TrafficSource() {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Box my={3}>
        <LinearProgressWithLabel label="Organic Search" value={65} />
      </Box>
      <Box my={3}>
        <LinearProgressWithLabel label="Referal" value={35} />
      </Box>
      <Box my={3}>
        <LinearProgressWithLabel label="Email" value={40} />
      </Box>
      <Box my={3}>
        <LinearProgressWithLabel label="Social" value={78} />
      </Box>
      <Box my={3}>
        <LinearProgressWithLabel label="Other" value={34} />
      </Box>
    </Box>
  );
}

function LinearProgressWithLabel(props) {
  const classes = useStyles();

  return (
    <Grid container display="flex" alignItems="center">
      <Grid item xs={3}>
        <Typography>{props.label}</Typography>
      </Grid>
      <Grid item xs={8} mr={1}>
        <LinearProgress
          className={classes.progressbar}
          variant="determinate"
          {...props}
        />
      </Grid>
      <Grid item xs={1}>
        <Typography
          align="right"
          variant="body2"
          color="textSecondary"
        >{`${Math.round(props.value)}%`}</Typography>
      </Grid>
    </Grid>
  );
}

export default TrafficSource;
