import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100%", 
    width: "100%"
  },
  progressbar:{
      height: "10px", 
      borderRadius: "5px"
  }
}));
export default useStyles;
