import {
  Area,
  AreaChart,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  ReferenceLine,
} from "recharts";

import numeral from "numeral";
import data from "../../../../utils/data";

const UsersChart = () => {
  return (
    <ResponsiveContainer width="100%" height={350}>
      <AreaChart data={data}>
        <defs>
          <linearGradient id="color" x1="0" y1="0" x2="0" y2="1">
            <stop offset="0%" stopColor="#30637a" stopOpacity={0.3} />
            <stop offset="75%" stopColor="#30637a" stopOpacity={0.02} />
          </linearGradient>
        </defs>

        <Area
          dataKey="uv"
          stroke="#30637a"
          fill="url(#color)"
          strokeWidth={2}
          activeDot={{ r: 8 }}
        />
        <ReferenceLine
          y={3500}
          // label={(<Typography>14K</Typography>}
          stroke="#30637a"
          // strokeDasharray="3 3"
        />

        <XAxis
          dataKey="name"
          tickSize={0.9}
          axisLine={true}
          tickLine={true}
          // tickFormatter={(str) => {
          // const date = parseISO(str);
          // if (date.getDate() % 7 === 0) {
          // return `Week ${date.getDate()}`;
          // }
          // return "1";
          // }
          // }
        />
        <Legend />
        <Tooltip />

        <YAxis
          label={{ value: "No of Views", angle: -90, position: "insideLeft" }}
          datakey="amt"
          axisLine={true}
          tickLine={true}
          tickCount={4}
          tickFormatter={(number) => numeral(number).format("0.0a")}
        />

        <CartesianGrid
          opacity={0.5}
          horizontal={false}
          vertical={true}
        />
      </AreaChart>
    </ResponsiveContainer>
  );
};
export default UsersChart;
