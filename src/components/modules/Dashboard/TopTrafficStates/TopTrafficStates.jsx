import React from "react";
import LinearProgress from "@material-ui/core/LinearProgress";
import { Box, Grid, Typography } from "@material-ui/core";
import useStyles from "./toptrafficstates_style";

function TopTrafficStates() {
  const classes = useStyles();
  const topTraffic = [
    {
      state: "Texas",
      value: 65,
    },
    {
      state: "Texas",
      value: 65,
    },
    {
      state: "Washington",
      value: 60,
    },
    {
      state: "Wyoming",
      value: 40,
    },
    {
      state: "Virginia",
      value: 78,
    },
    {
      state: "Others",
      value: 54,
    },
  ];

  return (
    <div className={classes.root}>
      {
        topTraffic.map((data, index)=>(
          <Box my={2} key={index}>
        <LinearProgressWithLabel label={data.state} value={data.value} />
      </Box>
        
      ))
    }
    </div>
  );
}

function LinearProgressWithLabel(props) {
  const classes = useStyles();

  return (
    <Grid container display="flex" justify="space-between" alignItems="center">
      <Grid item xs={3}>
        <Typography>{props.label}</Typography>
      </Grid>
      <Grid item xs={1}>
        <Typography
          align="right"
          variant="body2"
          color="textSecondary"
        >{`${Math.round(props.value)}%`}</Typography>
      </Grid>
      <Grid item xs={12} mr={1}>
        <LinearProgress
          className={classes.progressbar}
          variant="determinate"
          {...props}
        />
      </Grid>
    </Grid>
  );
}

export default TopTrafficStates;
