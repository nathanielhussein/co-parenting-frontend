import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
  },
  progressbar:{
      height: "10px", 
      borderRadius: "5px"
  }
}));
export default useStyles;
