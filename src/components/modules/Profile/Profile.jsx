import {
  Avatar,
  Box,
  Button,
  Divider,
  FormControl,
  FormHelperText,
  Grid,
  InputLabel,
  OutlinedInput,
  Paper,
  Typography,
} from "@material-ui/core";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import React, { useState } from "react";
import useStyles from "./profile_style";
import { useFormik } from "formik";
import * as Yup from "yup";
import { ButtonLoader } from "../../Loaders/Loader";
import clsx from "clsx";
import { updateUser } from "api/users";
import { useHistory } from "react-router-dom";
import { Alert } from "@material-ui/lab";
function Profile({ userData }) {
  console.log("user data is ", userData);
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [operationMessage, setOperationMessage] = useState({
    message: "",
    error: false,
  });
  const history = useHistory();
  const schema = Yup.object().shape({
    firstName: Yup.string().required("Required"),
    lastName: Yup.string().required("Required"),
    email: Yup.string().required("Required").email("Email Must be Valid!"),
    password: Yup.string().min(8, "Must be 8 characters or more"),
    confirm_password: Yup.string().when("password", {
      is: (val) => (val && val.length >= 0 ? true : false),
      then: Yup.string().oneOf(
        [Yup.ref("password")],
        "Password is not the same"
      ),
    }),
  });
  console.log("user firstname", userData.firstName);
  const formik = useFormik({
    initialValues: {
      firstName: userData.firstName || "",
      lastName: userData.lastName || "",
      email: userData.email || "",
      password: "",
      confirm_password: "",
    },
    validationSchema: schema,
    onSubmit: async (values) => {
      try {
        setLoading(true);
        const data = await updateUser(
          userData.id,
          values.firstName,
          values.lastName,
          values.email,
          values.password
        );
        console.log("data is ", data);
        setOperationMessage({
          error: false,
          message: "Profile updated successfully",
        });

        if (data.user) {
          setTimeout(() => {
            history.go("/dashboard");
          }, 1000);
          return;
        }
      } catch (error) {
        setOperationMessage({ error: true, message: error.message });
      }
      setLoading(false);
      formik.resetForm()
    },
  });

  const ShowMessage = ({ error, message }) => {
    return (
      <Alert
        variant="filled"
        severity={error ? "error" : "success"}
        className={`alert ${error ? "alert-danger" : "alert-success"} `}
      >
        <li className="">{message}</li>
      </Alert>
    );
  };

  return (
    <Paper className={classes.root}>
      <form className={classes.form} onSubmit={formik.handleSubmit}>
        <Grid container justify="space-around" spacing={1} alignItems="center">
          <Grid item xs={12} md={4}>
            <Box justifyContent="center" display="flex">
              <Avatar
                src="/images/avatar.jpeg"
                className={classes.profile_avatar}
              >
                N
              </Avatar>
            </Box>
            <Box my={2} display="flex" justifyContent="center">
              <FormControl variant="filled">
                <input
                  accept="image/*"
                  className={classes.file_input}
                  id="contained-button-file"
                  type="file"
                  placeholder="Choose Picture here"
                  onChange={formik.handleChange}
                  // onBlur={Boolean(formik.validateOnBlur)}
                  // value={formik.values.email}
                  // error={formik.errors.email}
                />
                <label htmlFor="contained-button-file">
                  <Button
                    startIcon={<CloudUploadIcon />}
                    variant="contained"
                    color="primary"
                    component="span"
                  >
                    Change Picture
                  </Button>
                </label>
              </FormControl>
            </Box>
          </Grid>
          <Grid item xs={12} md={5} spacing={2} container>
            {operationMessage.message !== "" && (
              <Grid item xs={12}>
                <ShowMessage {...operationMessage} />
              </Grid>
            )}
            <Grid item xs={12}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="firstName">
                  <Typography>First Name</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="filled"
                  style={{ width: "100%" }}
                >
                  <OutlinedInput
                    id="firstName"
                    name="firstName"
                    placeholder="Enter your firstName here"
                    onChange={formik.handleChange}
                    value={formik.values.firstName}
                    error={
                      formik.errors.firstName &&
                      Boolean(formik.errors.firstName)
                    }
                  />
                  <FormHelperText style={{ color: "red" }}>
                    {formik.touched.firstName && formik.errors.firstName ? (
                      <div>{formik.errors.firstName}</div>
                    ) : null}
                  </FormHelperText>
                </FormControl>
              </Box>
            </Grid>

            <Grid item xs={12}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="lastName">
                  <Typography>LastName</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="filled"
                  style={{ width: "100%" }}
                >
                  <OutlinedInput
                    id="lastName"
                    name="lastName"
                    placeholder="Enter your lastName here"
                    onChange={formik.handleChange}
                    value={formik.values.lastName}
                    error={
                      formik.errors.lastName && Boolean(formik.errors.lastName)
                    }
                  />
                  <FormHelperText style={{ color: "red" }}>
                    {formik.touched.lastName && formik.errors.lastName ? (
                      <div>{formik.errors.lastName}</div>
                    ) : null}
                  </FormHelperText>
                </FormControl>
              </Box>
            </Grid>

            <Grid item xs={12}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="email">
                  <Typography>Email Address</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="filled"
                  style={{ width: "100%" }}
                >
                  <OutlinedInput
                    id="email"
                    name="email"
                    type="email"
                    placeholder="Enter Email here"
                    onChange={formik.handleChange}
                    // onBlur={Boolean(formik.validateOnBlur)}
                    value={formik.values.email}
                    error={formik.errors.email && Boolean(formik.errors.email)}
                  />
                  <FormHelperText style={{ color: "red" }}>
                    {formik.touched.email && formik.errors.email ? (
                      <div>{formik.errors.email}</div>
                    ) : null}
                  </FormHelperText>
                </FormControl>
              </Box>
            </Grid>

            <Grid item xs={12}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="password">
                  <Typography>Password</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="filled"
                  style={{ width: "100%" }}
                >
                  <OutlinedInput
                    id="password"
                    name="password"
                    type="password"
                    placeholder="Enter Password here"
                    onChange={formik.handleChange}
                    // onBlur={Boolean(formik.validateOnBlur)}
                    value={formik.values.password}
                    error={
                      formik.errors.password && Boolean(formik.errors.password)
                    }
                  />
                  <FormHelperText style={{ color: "red" }}>
                    {formik.touched.password && formik.errors.password ? (
                      <div>{formik.errors.password}</div>
                    ) : null}
                  </FormHelperText>
                </FormControl>
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Box className={classes.form_group}>
                <InputLabel htmlFor="confirm">
                  <Typography>Confirm Address</Typography>
                </InputLabel>
                <FormControl
                  // size="small"
                  variant="filled"
                  style={{ width: "100%" }}
                >
                  <OutlinedInput
                    id="confirm"
                    name="confirm_password"
                    type="password"
                    placeholder="Enter Confirm here"
                    onChange={formik.handleChange}
                    value={formik.values.confirm_password}
                    error={
                      formik.errors.confirm_password &&
                      Boolean(formik.errors.confirm_password)
                    }
                  />
                  <FormHelperText style={{ color: "red" }}>
                    {formik.touched.confirm_password &&
                    formik.errors.confirm_password ? (
                      <div>{formik.errors.confirm_password}</div>
                    ) : null}
                  </FormHelperText>
                </FormControl>
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Divider light />
            </Grid>
            <Grid item xs={12} justify="center" container>
              <Button
                color="primary"
                variant="contained"
                fullWidth
                type="submit"
                className={clsx(classes.btn, classes.continueBtn)}
              >
                {loading ? (
                  <ButtonLoader color="#ffffff" size={10} />
                ) : (
                  "Save Changes"
                )}
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </form>
    </Paper>
  );
}

export default Profile;
