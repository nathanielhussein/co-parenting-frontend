import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    boxShadow: "0",
    minHeight: "100%",
    display: "flex",
    alignItems: "center",
  },
  form: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  profile_avatar: {
    width: "300px",
    height: "300px",
  },
  file_input: {
    display: "none",
  },
}));
export default useStyles;
