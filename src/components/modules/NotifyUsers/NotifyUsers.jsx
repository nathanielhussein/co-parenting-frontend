import { Box, InputBase } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import useStyles from "./notifyusers_style";

function NotifyUsers() {
  const classes = useStyles();

  return (
    <div>
        <Box
          className={classes.search}
          my={2}
          width="50%"
          color="text.secondary"
        >
          <div className={classes.searchField}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search orders..."
              value="nati"
              //   className={classnames(classes.inputRoot, classes.inputInput)}
              inputProps={{ "aria-label": "Search orders" }}
            />
          </div>
        </Box>
    </div>
  );
}

export default NotifyUsers;
