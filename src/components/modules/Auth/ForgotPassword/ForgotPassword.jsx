import {
  Box,
  Button,
  FormControl,
  FormHelperText,
  Grid,
  InputLabel,
  OutlinedInput,
  Paper,
  Typography,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import useStyles from "./forgotpassword_style";
import Alert from "@material-ui/lab/Alert";

import { ButtonLoader } from "../../../Loaders/Loader";
import { useAuthState } from "../../../../context";
import { Link, useHistory } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { forgotPassword } from "api/auth";


function ForgotPassword() {
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [forgotPasswordError, setForgotPasswordError] = useState("")
  const { auth_user } = useAuthState(); //read user details from context

  let history = useHistory();

  useEffect(() => {
    if (auth_user) {
      history.replace("/dashboard");
    }
  }, [auth_user, history]);

  const formik = useFormik({
    initialValues: {
      email: "",
    },
    validationSchema: Yup.object({
      email: Yup.string().email("Invalid email address").required("Required"),
    }),
    onSubmit: async (values) => {
      try {
        setLoading(true);
        const data = await forgotPassword(values.email);
        console.log("data is ", data);
        if (data) {
          history.replace("/auth/reset?email="+values.email);
          // return;
        }
      } catch (error) {
        setForgotPasswordError({ message: error.message });
      }
      setLoading(false);
    },
  });

  const ShowError = ({ errorMessage }) => {
    return (
      <Alert variant="filled" severity="error" className="alert alert-danger">
        <li className="">{errorMessage}</li>
      </Alert>
    );
  };

  return (
    <form onSubmit={formik.handleSubmit}>
      <Grid
        container
        className={classes.root}
        justify="center"
        alignItems="center"
      >
        <Grid item xs={12} sm={10} md={8} lg={5} xl={4}>
          <Paper className={classes.paper}>
            <Grid container justify="center" spacing={3}>
              {forgotPasswordError !== "" && (
                <Grid item xs={12}>
                  <ShowError errorMessage={forgotPasswordError.message} />
                </Grid>
              )}
              <Grid item xs={12} className={classes.logoContainer}>
                <img
                  src="/images/logo.svg"
                  alt="logo"
                  width={100}
                  height={100}
                />
              </Grid>
              <Grid item xs={10}>
                <Box mb={1}>
                  <InputLabel htmlFor="email">
                    <Typography>Email Address</Typography>
                  </InputLabel>
                </Box>
                <FormControl variant="filled" style={{ width: "100%" }}>
                  <OutlinedInput
                    // type="email"
                    id="email"
                    name="email"
                    placeholder="Type here"
                    onChange={formik.handleChange}
                    value={formik.values.email}
                    error={formik.errors.email}
                  />
                  <FormHelperText style={{ color: "red" }}>
                    {formik.touched.email && formik.errors.email ? (
                      <div>{formik.errors.email}</div>
                    ) : null}
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item xs={10}>
                <Button
                  fullWidth
                  type="submit"
                  variant="contained"
                  color="primary"
                  className={classes.submitBtn}
                  // disabled={loading}
                  style={{ cursor: loading ? "not-allowed" : "default" }}
                >
                  {loading ? (
                    <ButtonLoader color="#ffffff" size={10} />
                  ) : (
                    "Send"
                  )}
                </Button>
                
              </Grid>
              <Grid item xs={12} container justify="center" spacing={3}>
                <Grid item xs={4}>
                  <Link to="/auth/login" style={{ height: "100%" }}>
                    <Button size="small" fullWidth variant="outlined">
                      Back
                    </Button>
                  </Link>
                </Grid>
                <Grid item xs={4}>
                  <Link to="/auth/login">
                    <Button
                      size="small"
                      style={{ height: "100%" }}
                      fullWidth
                      variant="outlined"
                    >
                      Login
                    </Button>
                  </Link>
                </Grid>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </form>
  );
}

export default ForgotPassword;
