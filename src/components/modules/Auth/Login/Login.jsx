import {
  Box,
  Button,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Paper,
  Typography,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import useStyles from "./login_style";
import Alert from "@material-ui/lab/Alert";

import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import { ButtonLoader } from "../../../Loaders/Loader";
import { useAuthDispatch, loginUser, useAuthState } from "../../../../context";
import { Link, useHistory } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";

function Login() {
  const classes = useStyles();
  const [showPassword, setShowPassword] = useState(false);
  const [loginError, setLoginError] = useState("");
  const [loading, setLoading] = useState(false);
  const dispatch = useAuthDispatch();
  const { auth_user } = useAuthState(); //read user details from context

  let history = useHistory();

  useEffect(() => {
    if (auth_user) {
      history.replace("/dashboard");
    }
  }, [auth_user, history]);

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: Yup.object({
      email: Yup.string().email("Invalid email address").required("Required"),
      password: Yup.string().min(6, "Password must at least 6 character").required("Required"),
    }),
    onSubmit: async (values) => {
      try {
        setLoading(true);
        const data = await loginUser(dispatch, {
          email: values.email,
          password: values.password,
        });

        if (data.user) {
          history.go("/dashboard");
          return;
        }
      } catch (error) {
        setLoginError({ message: "Invalid Email or Password" });
      }
      setLoading(false);
    },
  });

  const ShowError = ({ errorMessage }) => {
    return (
      <Alert variant="filled" severity="error" className="alert alert-danger">
        <li className="">{errorMessage}</li>
      </Alert>
    );
  };

  const handleShowPassword = () => {
    setShowPassword((prev) => !prev);
  };

  return (
    <form onSubmit={formik.handleSubmit}>
      <Grid
        container
        className={classes.root}
        justify="center"
        alignItems="center"
      >
        <Grid item xs={12} sm={10} md={8} lg={5} xl={4}>
          <Paper className={classes.paper}>
            <Grid container justify="center" spacing={3}>
              {loginError !== "" && (
                <Grid item xs={12}>
                  <ShowError errorMessage={loginError.message} />
                </Grid>
              )}
              <Grid item xs={12} className={classes.logoContainer}>
                <img
                  src="/images/logo.svg"
                  alt="logo"
                  width={100}
                  height={100}
                />
              </Grid>
              <Grid item xs={10}>
                <Box mb={1}>
                  <InputLabel htmlFor="email">
                    <Typography>Email Address</Typography>
                  </InputLabel>
                </Box>
                <FormControl variant="filled" style={{ width: "100%" }}>
                  <OutlinedInput
                    type="email"
                    id="email"
                    name="email"
                    placeholder="Type here"
                    onChange={formik.handleChange}
                    value={formik.values.email}
                    error={formik.errors.email}
                  />
                  <FormHelperText style={{ color: "red" }}>
                    {formik.touched.email && formik.errors.email ? (
                      <div>{formik.errors.email}</div>
                    ) : null}
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item xs={10}>
                <Box mb={1}>
                  <InputLabel htmlFor="password">
                    <Typography>Password</Typography>
                  </InputLabel>
                </Box>
                <FormControl variant="filled" style={{ width: "100%" }}>
                  <OutlinedInput
                    id="password"
                    name="password"
                    placeholder="Type here"
                    type={showPassword ? "text" : "password"}
                    onChange={formik.handleChange}
                    value={formik.values.password}
                    error={formik.errors.password}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleShowPassword}
                        >
                          {!showPassword ? (
                            <Visibility color="primary" />
                          ) : (
                            <VisibilityOff color="primary" />
                          )}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                  <FormHelperText style={{ color: "red" }}>
                    {formik.touched.password && formik.errors.password ? (
                      <div>{formik.errors.password}</div>
                    ) : null}
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item xs={10}>
                <Button
                  fullWidth
                  type="submit"
                  variant="contained"
                  color="primary"
                  className={classes.submitBtn}
                  style={{ cursor: loading ? "not-allowed" : "default" }}
                >
                  {loading ? (
                    <ButtonLoader color="#ffffff" size={10} />
                  ) : (
                    "Login"
                  )}
                </Button>
              </Grid>
              <Grid item xs={12} container justify="space-around" spacing={3}>
                <Grid item xs={4}>
                  <Button
                    size="small"
                    style={{ height: "100%" }}
                    fullWidth
                    variant="outlined"
                  >
                    Back
                  </Button>
                </Grid>
                <Grid item xs={4}>
                  <Link to="/auth/forgot">
                    <Button
                      size="small"
                      style={{ height: "100%" }}
                      fullWidth
                      variant="outlined"
                    >
                      Forgot Password?
                    </Button>
                  </Link>
                </Grid>
                
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </form>
  );
}

export default Login;
