import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    minHeight: "100vh",
    textTransform: "none",
    width: "100%",
  },
  paper: {
    minHeight: "40vh",
    padding: "30px",
  },
  logoContainer: {
    display: "flex",
    justifyContent: "center",
    background: "#30637a !important",
    padding: "20px",
    borderRadius: "20px",
  },
  nav_items: {
    display: "flex",
    alignItems: "center",
  },
  submitBtn: {
    padding: "15px 20px",
  },
}));
export default useStyles;
