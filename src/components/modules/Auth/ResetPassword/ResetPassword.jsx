import {
  Box,
  Button,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Paper,
  Typography,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import useStyles from "./resetpassword_style";
import Alert from "@material-ui/lab/Alert";

import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import { ButtonLoader } from "../../../Loaders/Loader";
import { useAuthState } from "../../../../context";
import { useHistory, useLocation } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { forgotPassword, resetPassword } from "api/auth";
import clsx from "clsx";

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

function ResetPassword() {
  const classes = useStyles();
  const [resetPasswordError, setResetPasswordError] = useState("");
  const [resentOTP, setResentOTP] = useState("");
  const [loading, setLoading] = useState(false);
  const { auth_user } = useAuthState(); //read user details from context
  const [showPassword, setShowPassword] = useState(false);

  let query = useQuery();

  let history = useHistory();

  useEffect(() => {
    if (auth_user) {
      history.replace("/dashboard");
    }
  }, [auth_user, history]);

  const schema = Yup.object().shape({
    password: Yup.string()
      .min(8, "Must be 8 characters or more")
      .required("Required"),
    confirm_password: Yup.string().when("password", {
      is: (val) => (val && val.length > 0 ? true : false),
      then: Yup.string().oneOf(
        [Yup.ref("password")],
        "Password is not the same"
      ),
    }),
  });
  const formik = useFormik({
    initialValues: {
      password: "",
      confirm_password: "",
      resetCode: "",
      email: query.get("email"),
    },
    validationSchema: schema,
    onSubmit: async (values) => {
      if (values.password !== values.confirm_password) {
        setResetPasswordError({ message: "Password is not the same" });
        return;
      }
      try {
        setLoading(true);
        const data = await resetPassword(
          values.email,
          values.resetCode,
          values.password
        );
        console.log("data is ", data);
        if (data.statusCode === 200) {
          history.replace("/auth/login");
        } else {
          setResetPasswordError({ message: data.data.message });
          setTimeout(() => {
            history.replace("/auth/login");
          }, 1000);
        }
      } catch (error) {
        setResetPasswordError({ message: error.message });
      }
      setLoading(false);
    },
  });

  const ShowMessage = ({ message, type }) => {
    return (
      <Alert
        variant="filled"
        severity={type === "error" ? "error" : "success"}
        className={clsx(
          "alert",
          type === "error" ? "alert-danger" : "alert-success"
        )}
      >
        <li className="">{message}</li>
      </Alert>
    );
  };

  const handleResendOTP = async () => {
    try {
      setLoading(true);
      console.log(query.get("email"), "is the email");
      const data = await forgotPassword(query.get("email"));
      setResentOTP("OTP has sent successfully");
      console.log("data is ", data);
      if (data.statusCode === 200) {
        setResentOTP({ message: "Reset Password OTP has been resent" });
      }
    } catch (error) {
      setResetPasswordError({ message: error.message });
    }
    setLoading(false);
  };

  const handleShowPassword = () => {
    setShowPassword((prev) => !prev);
  };

  return (
    <form onSubmit={formik.handleSubmit}>
      <Grid
        container
        className={classes.root}
        justify="center"
        alignItems="center"
      >
        <Grid item xs={12} sm={10} md={8} lg={5} xl={4}>
          <Paper className={classes.paper}>
            <Grid container justify="center" spacing={3}>
              {resetPasswordError !== "" && (
                <Grid item xs={12}>
                  <ShowMessage
                    type="error"
                    message={resetPasswordError.message}
                  />
                </Grid>
              )}
              {resentOTP !== "" && (
                <Grid item xs={12}>
                  <ShowMessage type="success" message={resentOTP} />
                </Grid>
              )}
              <Grid item xs={12} className={classes.logoContainer}>
                <img
                  src="/images/logo.svg"
                  alt="logo"
                  width={100}
                  height={100}
                />
              </Grid>
              <Grid item xs={10}>
                <Box mb={1}>
                  <InputLabel htmlFor="resetCode">
                    <Typography>Reset Code</Typography>
                  </InputLabel>
                </Box>
                <FormControl variant="filled" style={{ width: "100%" }}>
                  <OutlinedInput
                    id="resetCode"
                    name="resetCode"
                    placeholder="Enter the OTP here"
                    onChange={formik.handleChange}
                    value={formik.values.resetCode}
                    error={Boolean(formik.errors.resetCode)}
                  />
                  <FormHelperText style={{ color: "red" }}>
                    {formik.touched.resetCode && formik.errors.resetCode ? (
                      <div>{formik.errors.resetCode}</div>
                    ) : null}
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item xs={10}>
                <Box mb={1}>
                  <InputLabel htmlFor="password">
                    <Typography>Password</Typography>
                  </InputLabel>
                </Box>
                <FormControl variant="filled" style={{ width: "100%" }}>
                  <OutlinedInput
                    id="password"
                    name="password"
                    placeholder="Type you password here"
                    type={showPassword ? "text" : "password"}
                    onChange={formik.handleChange}
                    value={formik.values.password}
                    error={Boolean(formik.errors.password)}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleShowPassword}
                        >
                          {!showPassword ? (
                            <Visibility color="primary" />
                          ) : (
                            <VisibilityOff color="primary" />
                          )}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                  <FormHelperText style={{ color: "red" }}>
                    {formik.touched.password && formik.errors.password ? (
                      <div>{formik.errors.password}</div>
                    ) : null}
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item xs={10}>
                <Box mb={1}>
                  <InputLabel htmlFor="confirm_password">
                    <Typography>Confirm</Typography>
                  </InputLabel>
                </Box>
                <FormControl variant="filled" style={{ width: "100%" }}>
                  <OutlinedInput
                    id="confirm_password"
                    name="confirm_password"
                    placeholder="Confirm your password here"
                    type={showPassword ? "text" : "password"}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.confirm_password}
                    error={Boolean(formik.errors.confirm_password)}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleShowPassword}
                        >
                          {!showPassword ? (
                            <Visibility color="primary" />
                          ) : (
                            <VisibilityOff color="primary" />
                          )}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                  <FormHelperText style={{ color: "red" }}>
                    {formik.touched.confirm_password &&
                    formik.errors.confirm_password ? (
                      <div>{formik.errors.confirm_password}</div>
                    ) : null}
                  </FormHelperText>
                </FormControl>
              </Grid>

              <Grid item xs={10} container justify="flex-end">
                <Button
                  onClick={handleResendOTP}
                  style={{ cursor: loading ? "not-allowed" : "default" }}
                >
                  <Typography align="right" style={{ color: "blue" }}>
                    Resend
                  </Typography>
                </Button>
              </Grid>
              <Grid item xs={10}>
                <Button
                  fullWidth
                  type="submit"
                  variant="contained"
                  color="primary"
                  className={classes.submitBtn}
                  //   onClick={handleSubmit}
                  // disabled={loading}
                  style={{ cursor: loading ? "not-allowed" : "default" }}
                >
                  {loading ? (
                    <ButtonLoader color="#ffffff" size={10} />
                  ) : (
                    "Submit"
                  )}
                </Button>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </form>
  );
}

export default ResetPassword;
