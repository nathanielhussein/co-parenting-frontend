import { Box } from "@material-ui/core";
import { getAccessGroupList } from "api/teams";
import AddEditAccessGroup from "components/Modals/AddEditAccessGroup/AddEditAccessGroup";
import AccessGroupTable from "components/modules/Team/AccessGroupTable/AccessGroupTable";
import AllTeamMemberDetail from "components/modules/Team/AllTeamMemberDetail/AllTeamMemberDetail";
import SingleAccessGroupDetail from "components/modules/Team/SingleAccessGroupDetail/SingleAccessGroupDetail";
import PageNotFound from "pages/404/404";
import React, { useEffect, useState } from "react";
import { Route, Switch, useRouteMatch } from "react-router-dom";

function Team() {
  let { path } = useRouteMatch();

  const [accessGroups, setAccessGroups] = useState([]);

  function createData(id, group_name) {
    return {
      id,
      group_name: group_name.toString().toUpperCase(),
    };
  }
  useEffect(() => {
    const fetchAccessGroupList = async () => {
      try {
        const data = await getAccessGroupList();
        if (data) {
          const result = data.map((role, index) => {
            return createData(index + 1, role.roleName);
          });
          setAccessGroups(result);
        }
      } catch (error) {
        console.log("fetching data error");
      }
    };

    return fetchAccessGroupList();
  }, []);

  return (
    <Switch>
      <Route exact path={`${path}/team-members`}>
        <AllTeamMemberDetail />
      </Route>
      <Route exact path={`${path}/access-groups`}>
        <Box>
          <Box display="flex" justifyContent="flex-end">
            <AddEditAccessGroup accessGroups={accessGroups} />
          </Box>
          <AccessGroupTable />
        </Box>
      </Route>
      {accessGroups &&
        accessGroups.map((group, index) => (
          <Route
            key={index}
            exact
            path={`${path}/access-groups/${group.group_name.toLowerCase()}`}
          >
            <SingleAccessGroupDetail group={group} />
          </Route>
        ))}
      <Route path="*" component={PageNotFound} />
    </Switch>
  );
}

export default Team;

// import { Box, Grid, Tab, Tabs } from "@material-ui/core";
// import React, { useEffect, useState } from "react";
// import AccessGroupTable from "./AccessGroupTable/AccessGroupTable";
// import useStyles from "./team_style";
// import AddEditAccessGroup from "../../Modals/AddEditAccessGroup/AddEditAccessGroup";
// import SingleAccessGroupDetail from "./SingleAccessGroupDetail/SingleAccessGroupDetail";
// import {
//   Route,
//   Switch,
//   useHistory,
//   useLocation,
//   useRouteMatch,
// } from "react-router";
// import PageNotFound from "../../../pages/404/404";
// import AllTeamMemberDetail from "./AllTeamMemberDetail/AllTeamMemberDetail";
// import { getAccessGroupList } from "api/teams";

// const Team = () => {
//   let { path } = useRouteMatch();
//   const classes = useStyles();
//   const [selectedTab, setSelectedTab] = useState(0);
//   let location = useLocation();
//   const history = useHistory();

//   useEffect(() => {
//     if (location.pathname.length > 5) {
//       setSelectedTab(1);
//     }
//     console.log("path is ", location.pathname);
//   }, [location]);

//   const [accessGroups, setAccessGroups] = useState([]);

//   const handleChange = (event, newValue) => {
//     if (location.pathname.length > 5) {
//       history.goBack();
//     }
//     setSelectedTab(newValue);
//   };

//   function createData(id, group_name) {
//     return {
//       id,
//       group_name: group_name.toString().toLowerCase(),
//     };
//   }

//   useEffect(() => {
//     const fetchAccessGroupList = async () => {
//       try {
//         const data = await getAccessGroupList();
//         if (data) {
//           const result = data.map((role, index) => {
//             return createData(index + 1, role.roleName);
//           });
//           console.log(result, " is the access group");
//           setAccessGroups(result);
//         }
//       } catch (error) {
//         console.log("fetching data error");
//       }
//     };

//     return fetchAccessGroupList();
//   }, []);

//   function TabPanel(props) {
//     const { children, value, index, ...other } = props;

//     return (
//       <div
//         role="tabpanel"
//         hidden={value !== index}
//         id={`simple-tabpanel-${index}`}
//         aria-labelledby={`simple-tab-${index}`}
//         {...other}
//       >
//         {value === index && children}
//       </div>
//     );
//   }

//   return (
//     <div>
//       <Box my={2}>
//         <Grid
//           container
//           style={{ borderBottom: "1px solid rgba(0, 0, 0, 0.1)" }}
//           spacing={2}
//           justify="space-between"
//         >
//           <Grid item xs={12} md={6} className={classes.tabs}>
//             <Tabs
//               value={selectedTab}
//               onChange={handleChange}
//               aria-label="order status"
//               indicatorColor="primary"
//             >
//               <Tab
//                 label="All members"
//                 id="all_members"
//                 aria-controls="all members"
//               />
//               <Tab
//                 label="Access group"
//                 id="access_group"
//                 aria-controls="access group"
//               />
//             </Tabs>
//           </Grid>

//           <Grid item xs={12}>
//             <TabPanel value={selectedTab} index={0}>
//             </TabPanel>
//             <TabPanel value={selectedTab} index={1}>
//               <Switch>
//                 <Route exact path={`${path}`}>
//                   <React.Fragment>

//                     <AccessGroupTable />
//                   </React.Fragment>
//                 </Route>
//                 {accessGroups && accessGroups.map((group, index) => (
//                   <Route
//                     key={index}
//                     exact
//                     path={`${path}/${group.group_name.toLowerCase()}`}
//                   >
//                     <SingleAccessGroupDetail group={group} />
//                   </Route>
//                 ))}
//                 <Route path="*" component={PageNotFound} />
//               </Switch>
//             </TabPanel>
//           </Grid>
//         </Grid>
//       </Box>
//     </div>
//   );
// };

// export default Team;
