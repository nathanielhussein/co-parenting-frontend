import {
  Box,
  Button,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useHistory, useRouteMatch } from "react-router";
import useStyles from "./accessgrouptable_style";
import { deleteAccessGroup, getAccessGroupList } from "api/teams";
import { NormalLoader } from "components/Loaders/Loader";
import DeleteConfirmation from "components/Modals/DeleteConfirmation/DeleteConfirmation";
import AddEditAccessGroup from "components/Modals/AddEditAccessGroup/AddEditAccessGroup";

function AccessGroupTable() {
  const classes = useStyles();
  const history = useHistory();
  let { url } = useRouteMatch();
  const [rows, setRows] = useState([]);
  const [loading, setLoading] = useState(false);

  function createData(id, roleName) {
    return {
      id,
      roleName,
    };
  }

  useEffect(() => {
    const fetchAccessGroupList = async () => {
      try {
        setLoading(true);
        const data = await getAccessGroupList();
        if (data) {
          const result = data.map((role, index) => {
            return createData(role.id, role.roleName);
          });
          console.log("dat is ", data);
          setRows(result);
        }
      } catch (error) {
        console.log("fetching data error");
      }
      setLoading(false);
    };

    return fetchAccessGroupList();
  }, []);

  const handleDelete = async (role_id, roleName) => {
    // console.log(`role id ${role_id} role ${roleName}`)
    try {
      setLoading(true);
      const data = await deleteAccessGroup(role_id, roleName);
      if (data) {
        console.log("user deleted successfully");
      }
    } catch (error) {
      console.log("there is an error on deleting user", error);
    }
    setLoading(false);
  };

  const SingleRow = ({ id, roleName, index }) => {
    const handleClick = (roleName) => () => {
      history.push(`${url}/${roleName}`);
    };
    return (
      <TableRow className={classes.userrow} key={id}>
        <TableCell component="th" scope="row">
          {index + 1}
        </TableCell>
        <TableCell
          style={{ width: "100%", display: "flex" }}
          align="left"
        >
          <Typography>{roleName}</Typography>
        </TableCell>
        <TableCell align="left"></TableCell>
        <TableCell align="left"></TableCell>
        <TableCell align="left"></TableCell>
        <TableCell align="left"></TableCell>
        <TableCell align="left"></TableCell>
        <TableCell align="left"></TableCell>
        <TableCell align="left"></TableCell>
        <TableCell align="left"></TableCell>
        <TableCell align="left"></TableCell>
        <TableCell align="left"></TableCell>
        <TableCell align="left"></TableCell>
        <TableCell align="left"></TableCell>
        <TableCell align="left"></TableCell>
        <TableCell align="center" style={{ display: "flex" }}>
          <Box display="flex" justifyContent="center">
            <Box mx={1}>
              <Button
                className={classes.header_actions_addBtn}
                variant="outlined"
                color="primary"
                onClick={handleClick(roleName)}
              >
                View Members
              </Button>
            </Box>
          </Box>
        </TableCell>
        <TableCell align="center">
          <Box>
            <AddEditAccessGroup isEditing accessGroup={{id, roleName}} />
          </Box>
          {/* <Box display="flex" justifyContent="center"> */}
          {/* <Box mx={1}> */}
          {/* <Button */}
          {/* className={classes.header_actions_addBtn} */}
          {/* variant="outlined" */}
          {/* color="primary" */}
          {/* onClick={handleClick(id, roleName)} */}
          {/* > */}
          {/* Edit */}
          {/* </Button> */}
          {/* </Box> */}
          {/* </Box> */}
        </TableCell>
        <TableCell align="center">
          <Box mx={1}>
            <DeleteConfirmation
              handleDelete={() => handleDelete(id, roleName)}
              message={`Are you sure you want to delete group${roleName}`}
            />
          </Box>
        </TableCell>
      </TableRow>
    );
  };

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead className={classes.table_header}>
          <TableRow>
            <TableCell align="left">#</TableCell>
            <TableCell
              style={{ width: "100%", display: "flex" }}
              align="left"
            >
              GROUP NAME
            </TableCell>
            <TableCell align="left"></TableCell>
            <TableCell align="left"></TableCell>
            <TableCell align="left"></TableCell>
            <TableCell align="left"></TableCell>
            <TableCell align="left"></TableCell>
            <TableCell align="left"></TableCell>
            <TableCell align="left"></TableCell>
            <TableCell align="left"></TableCell>
            <TableCell align="left"></TableCell>
            <TableCell align="left"></TableCell>
            <TableCell align="left"></TableCell>
            <TableCell align="left"></TableCell>
            <TableCell align="left"></TableCell>
            <TableCell align="left"></TableCell>
            <TableCell align="center">View Members</TableCell>
            <TableCell align="center">Edit Group</TableCell>
            <TableCell align="center">Delete Group</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {!loading ? (
            <React.Fragment>
              {rows.map((row, index) => (
                <SingleRow key={index} index={index} {...row} />
              ))}
            </React.Fragment>
          ) : (
            <TableRow>
              <TableCell align="center" colSpan={10}>
                <NormalLoader />
              </TableCell>
            </TableRow>
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default AccessGroupTable;
