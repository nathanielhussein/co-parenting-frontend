import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  userrow: {
    "&:hover": {
      cursor: "pointer",
      backgroundColor: theme.palette.action.hover,
    },
  },
  table_header: {
    backgroundColor: "#eee",
  },
  header_actions_addBtn: {
    minWidth: "150px",
    borderRadius: "50px",
  },
  table_body:{
    minHeight:"30vh", 
    width: "100%",
    border: "1px solid red", 
    display: "flex", 
    alignItems: "center", 
    justifyContent: "center"
  },
}));
export default useStyles;
