import {
  Box,
  Grid,
  IconButton,
  InputBase,
  Typography,
} from "@material-ui/core";
import { ArrowBackSharp } from "@material-ui/icons";
import React, { useEffect, useState } from "react";
import AllMemberTable from "../AllMemberTable/AllMemberTable";
import useStyles from "./singleaccessgroup_style";
import SearchIcon from "@material-ui/icons/Search";
import { useHistory } from "react-router";
import { getRoleTeamMembers } from "api/teams";
import { getCreatedDate } from "utils/functions";
import { NormalLoader } from "components/Loaders/Loader";

function SingleAccessGroupDetail({ group }) {
  const history = useHistory();
  const classes = useStyles();
  const [searchText, setsearchText] = useState("");
  const [loading, setLoading] = useState(false);
  const [rows, setRows] = useState([]);

  const handleBack = () => {
    history.goBack();
  };

  function createData(
    id,
    team_member,
    email,
    phone_number,
    address,
    role,
    access_group,
    date_added
  ) {
    return {
      id,
      team_member,
      email,
      phone_number,
      address,
      role,
      access_group,
      date_added,
    };
  }
  let name = group.group_name;

  useEffect(() => {
    const fetchUsersData = async () => {
      try {
        setLoading(true);
        const data = await getRoleTeamMembers(name);
        console.log("the data is ", data);
        if (data !== null) {
          const result = data.map((user, index) => {
            return createData(
              index + 1,
              `${user.firstName} ${user.lastName}`,
              user.id,
              "+145094239232",
              user.verified,
              getCreatedDate(user.created_date)
            );
          });
          console.log("result is ", result);
          setRows(result);
          // setFilterdRow(result);
        }
      } catch (error) {
        console.log("fetching data error");
      }
      setLoading(false);
    };
    return fetchUsersData();
  }, [name]);

  const [filterdRow, setFilterdRow] = useState(rows);

  useEffect(() => {
    let searchedResult = rows.filter(
      (row) =>
        row.team_member.toLowerCase().includes(searchText) ||
        row.email.toLowerCase().includes(searchText)
    );
    setFilterdRow(searchedResult);
  }, [searchText, rows]);

  return (
    <div>
      <Grid container spacing={3}>
        <Grid
          item
          xs={12}
          container
          alignItems="center"
          justify="space-between"
        >
          <Box display="flex" alignItems="center">
            <IconButton onClick={handleBack}>
              <ArrowBackSharp />
            </IconButton>
            <Box mx={1}>
              <Typography
                style={{ textTransform: "uppercase" }}
                color="primary"
                variant="h6"
              >
                {group.group_name}
              </Typography>
            </Box>
          </Box>
          <Box
            display="flex"
            alignItems="center"
            className={classes.header_search}
            mx={4}
          >
            <Box className={classes.search_searchField}>
              <SearchIcon />
            </Box>
            <Box mx={2} width="100%">
              <InputBase
                fullWidth
                value={searchText}
                onChange={(e) => setsearchText(e.target.value)}
                placeholder="Search..."
                inputProps={{ "aria-label": "Search" }}
              />
            </Box>
          </Box>
        </Grid>
        <Grid item xs={12}>
          {!loading ? (
            <AllMemberTable accessGroups={rows} rows={filterdRow} />
          ) : (
            <NormalLoader />
          )}
        </Grid>
      </Grid>
    </div>
  );
}

export default SingleAccessGroupDetail;
