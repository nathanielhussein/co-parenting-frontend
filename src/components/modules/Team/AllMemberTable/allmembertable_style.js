import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  table: {
    minHeight: "300px"
  },
  userrow: {
    "&:hover": {
      cursor: "pointer",
      backgroundColor: theme.palette.action.hover,
    },

  },
  table_header: {
    backgroundColor: "#eee",
  },
}));
export default useStyles;
