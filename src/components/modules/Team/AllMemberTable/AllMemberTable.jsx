import {
  Avatar,
  Box,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import { deleteTeamMember } from "api/teams";
import DeleteConfirmation from "components/Modals/DeleteConfirmation/DeleteConfirmation";
import React from "react";
import AddEditTeamMember from "../../../Modals/AddEditTeamMember/AddEditTeamMember";
import useStyles from "./allmembertable_style";

function AllMemberTable({ rows, accessGroups }) {
  const classes = useStyles();

  const handleDelete = async (userId) => {
    try {
      const data = await deleteTeamMember(userId);
      if (data) {
        console.log("user deleted successfully");
      }
    } catch (error) {
      console.log("there is an error on deleting user", error);
    }
  };

  const SingleMemberRow = ({
    id,
    team_member,
    firstName, 
    lastName,
    email,
    phone_number,
    address,
    role,
    date_added,
    index
  }) => {
    // const handleClick = (id) => {
    //   history.push(`${url}/${id}`);
    // };
    return (
      <TableRow className={classes.userrow} key={id}>
        <TableCell component="th" scope="row">
          {index + 1}
        </TableCell>
        <TableCell align="left">
          <Box display="flex" alignItems="center">
            <Box mx={1}>
              <Avatar src="/images/friend1.jpg" />
            </Box>
            <Box mx={1}>
              <Typography>{team_member}</Typography>
            </Box>
          </Box>
        </TableCell>
        <TableCell align="left">{email}</TableCell>
        <TableCell align="left">{phone_number}</TableCell>
        <TableCell align="left">{address}</TableCell>
        <TableCell align="left">{role}</TableCell>
        <TableCell align="left">{date_added}</TableCell>
        <TableCell align="center">
          <Box display="flex" justifyContent="center">
            <Box mx={1}>
              <AddEditTeamMember
                accessGroups={accessGroups}
                userData={{
                  id,
                  firstName,
                  lastName,
                  email,
                  phone_number,
                  roleName: role,
                  date_added,
                }}
              />
            </Box>
            <Box mx={1}>
              <DeleteConfirmation handleDelete={() => handleDelete(id)} />
            </Box>
          </Box>
        </TableCell>
      </TableRow>
    );
  };

  return (
    <>
      <TableContainer className={classes.table} component={Paper}>
        <Table aria-label="simple table">
          <TableHead className={classes.table_header}>
            <TableRow>
              <TableCell align="left">ID</TableCell>
              <TableCell align="left">Team Members</TableCell>
              <TableCell align="left">EMAIL</TableCell>
              <TableCell align="left">PHONE NUMBER</TableCell>
              <TableCell align="left">ADDRESS</TableCell>
              <TableCell align="left">ROLE</TableCell>
              <TableCell align="left">DATE ADDED</TableCell>
              <TableCell align="center">ACTIONS</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows &&
              rows.map((row, index) => (
                <SingleMemberRow key={index} index={index} {...row} />
              ))}
          </TableBody>
        </Table>
      </TableContainer>
     
    </>
  );
}

export default AllMemberTable;
