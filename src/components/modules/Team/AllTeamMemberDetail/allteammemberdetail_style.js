import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    boxShadow: "0",
  },
  header: {
    marginBottom: "30px",
  },

  header_search: {
    backgroundColor: "white",
    // minWidth: "85%",
    flex: "1",
    padding: "5px 20px",
    borderRadius: "20px",
  },
  search_searchField: {
    display: "flex",
  },
}));
export default useStyles;
