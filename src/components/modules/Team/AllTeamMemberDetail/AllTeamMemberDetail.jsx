import React from "react";
import AllMemberTable from "../AllMemberTable/AllMemberTable";
import { Box, InputBase } from "@material-ui/core";
import { useEffect, useState } from "react";
import SearchIcon from "@material-ui/icons/Search";
import useStyles from "./allteammemberdetail_style";
import { getAllMembers } from "api/teams";
import { getCreatedDate } from "utils/functions";
import { NormalLoader } from "components/Loaders/Loader";
import AddEditTeamMember from "components/Modals/AddEditTeamMember/AddEditTeamMember";
import { Pagination } from "@material-ui/lab";
function AllTeamMemberDetail({ accessGroups }) {
  const [searchText, setsearchText] = useState("");
  const [loading, setLoading] = useState(false);
  const [rows, setRows] = useState([]);
  const [filterdRow, setFilterdRow] = useState(rows);
  const [currentPage, setCurrentPage] = useState(1);
  const [rowPerPage] = useState(5);

  const handlePaginate = (event, value) => {
    console.log("page value is ", value);
    setCurrentPage(value);
  };

  const classes = useStyles();

  function createData(
    id,
    team_member,
    firstName, 
    lastName,
    email,
    phone_number,
    address,
    role,
    date_added
  ) {
    return {
      id,
      team_member,
      firstName, 
      lastName,
      email,
      phone_number,
      address,
      role,
      date_added,
    };
  }

  useEffect(() => {
    const fetchUsersData = async () => {
      try {
        setLoading(true);
        const data = await getAllMembers(currentPage, rowPerPage);
        if (data !== null) {
          const result = data.map((user, index) => {
            return createData(
              user.id,
              `${user.firstName} ${user.lastName}`,
              user.firstName, 
              user.lastName,
              user.email,
              "+145094239232",
              user.email,
              user.userRole,
              getCreatedDate(user.created_date)
            );
          });
          console.log("result is ", result);
          setRows(result);
          // setFilterdRow(result);
        }
        // setLoading((prev) => !prev);
      } catch (error) {
        console.log("fetching data error");
        // setLoading(prev=> !prev);
      }
      setLoading(false);
    };
    return fetchUsersData();
  }, [currentPage, rowPerPage]);

  useEffect(() => {
    let searchedResult = rows.filter((row) => {
      return (
        row.team_member.toLowerCase().includes(searchText) ||
        row.email.toLowerCase().includes(searchText)
      );
    });
    setFilterdRow(searchedResult);
  }, [searchText, rows]);

  return (
    <div>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        className={classes.header}
      >
        <Box
          display="flex"
          alignItems="center"
          className={classes.header_search}
        >
          <Box className={classes.search_searchField}>
            <SearchIcon />
          </Box>
          <Box mx={2} width="100%">
            <InputBase
              fullWidth
              value={searchText}
              onChange={(e) => setsearchText(e.target.value)}
              placeholder="Search..."
              inputProps={{ "aria-label": "Search" }}
            />
          </Box>
        </Box>
        <Box display="flex" mx={2} className={classes.header_actions}>
          <AddEditTeamMember accessGroups={accessGroups} />
        </Box>
      </Box>
      <Box>
        {loading ? (
          <Box display="flex" justifyContent="center">
            <NormalLoader />
          </Box>
        ) : (
          <>
            <AllMemberTable accessGroups={accessGroups} rows={filterdRow} />
            <Box my={4} display="flex" justifyContent="center">
              <Pagination
                showFirstButton
                showLastButton
                color="primary"
                size="large"
                count={parseInt(rows.length / rowPerPage + 1, 10)}
                page={currentPage}
                onChange={handlePaginate}
              />
            </Box>
          </>
        )}
      </Box>
    </div>
  );
}

export default AllTeamMemberDetail;
