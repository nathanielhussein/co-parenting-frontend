import { Grid } from '@material-ui/core'
import React from 'react'
import Navbar from '../NavBar/Navbar'
import Sidebar from '../Sidebar/Sidebar'
import useStyles from "./baselayout_style"

function BaseLayout({ children }) {
   
    const classes = useStyles()
    return (
        <Grid container>
            <Grid item xs={12} className={classes.navbar}>
                <Navbar />
            </Grid>
            <Grid item xs={1} md={2} className={classes.sidebar}>
                <Sidebar />
            </Grid>
            <Grid item xs={11} md={10} className={classes.mainContent}>
                {children}
            </Grid>
        </Grid>
    )
}

export default BaseLayout
