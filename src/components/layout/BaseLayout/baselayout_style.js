import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    minHeight: "90vh",
  },
  navbar: {
    //   padding: "10px 20px"
  },
  sidebar: {
   
  },
  mainContent: {
      maxHeight: "calc(100vh - 70px)",
      overflowY: "auto",
      overflowX: "hidden",
      maxWidth: "100%", 
      padding: "20px 40px"
  }
}));
export default useStyles;
