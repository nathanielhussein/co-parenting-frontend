import { useEffect, useState } from "react";
import {
  Box,
  Divider,
  Hidden,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Paper,
  Typography,
} from "@material-ui/core";

// import SettingsIcon from "@material-ui/icons/Settings";
import DashboardIcon from "@material-ui/icons/Dashboard";
import GroupIcon from "@material-ui/icons/Group";
import clsx from "clsx";
import { Link, useLocation } from "react-router-dom";
import useStyles from "./sidebar_style";
import Logout from "../../Modals/Logout/Logout";
import NotificationsActiveIcon from "@material-ui/icons/NotificationsActive";
import { ExpandLess, People } from "@material-ui/icons";
import ExpandMore from "@material-ui/icons/ExpandMore";
import Collapse from "@material-ui/core/Collapse";
import HowToRegIcon from "@material-ui/icons/HowToReg";

function Sidebar() {
  const classes = useStyles();

  const [currentPathName, setCurrentPathName] = useState("/dashboard");
  const [teamDropdownListOpen, setTeamDropdownListOpen] = useState(false);

  let location = useLocation();

  useEffect(() => {
    setCurrentPathName(location.pathname);
  }, [location]);

  return (
    <Paper className={classes.root}>
      <Box>
        <List component="nav" aria-label="main side bar menus">
          <Link to="/dashboard">
            <ListItem
              button
              selected={currentPathName === "/dashboard" ? true : false}
              className={clsx(
                classes.list_item,
                currentPathName.includes("/dashboard")
                  ? classes.list_item_active
                  : null
              )}
            >
              <ListItemIcon>
                <DashboardIcon
                  color={
                    currentPathName.includes("/dashboard")
                      ? "primary"
                      : "secondary"
                  }
                />
              </ListItemIcon>
              <Hidden only={["sm", "xs", "md"]}>
                <ListItemText>
                  <Typography color="textSecondary" variant="button">
                    Dashboard
                  </Typography>
                </ListItemText>
              </Hidden>
            </ListItem>
          </Link>
          <Divider light />
          <Link to="/users">
            <ListItem
              button
              selected={currentPathName.includes("/users") ? true : false}
              className={clsx(
                classes.list_item,
                currentPathName.includes("/users")
                  ? classes.list_item_active
                  : null
              )}
            >
              <ListItemIcon>
                <GroupIcon
                  color={
                    currentPathName.includes("/users") ? "primary" : "secondary"
                  }
                />
              </ListItemIcon>
              <Hidden only={["sm", "xs", "md"]}>
                <ListItemText>
                  <Typography color="textSecondary" variant="button">
                    Users
                  </Typography>
                </ListItemText>
              </Hidden>
            </ListItem>
          </Link>
          <Divider light />
          <ListItem
            button
            selected={currentPathName.includes("/team") ? true : false}
            onClick={() => {
              setTeamDropdownListOpen(
                (teamDropdownListOpen) => !teamDropdownListOpen
              );
            }}
          >
            <ListItemIcon>
              <DashboardIcon
                color={
                  currentPathName.includes("/team") ? "primary" : "secondary"
                }
              />
            </ListItemIcon>
            <Hidden only={["sm", "xs", "md"]}>
              <ListItemText>
                <Typography color="textSecondary" variant="button">
                  Team
                </Typography>
              </ListItemText>{" "}
            </Hidden>
            {teamDropdownListOpen ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
          <Collapse in={teamDropdownListOpen} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              <Link to="/team/team-members">
                <ListItem
                  button
                  selected={
                    currentPathName.includes("/team/team-members")
                      ? true
                      : false
                  }
                  className={clsx(
                    classes.list_item,
                    classes.nested,
                    currentPathName.includes("/team/team-members")
                      ? classes.list_item_active
                      : null
                  )}
                >
                  <ListItemIcon>
                    <People />
                  </ListItemIcon>
                  <ListItemText primary="All Member" />
                </ListItem>
              </Link>
              <Link to="/team/access-groups">
                <ListItem
                  button
                  selected={
                    currentPathName.includes("/team/access-groups")
                      ? true
                      : false
                  }
                  className={clsx(
                    classes.list_item,
                    classes.nested,
                    currentPathName.includes("/team/access-groups")
                      ? classes.list_item_active
                      : null
                  )}
                >
                  <ListItemIcon>
                    <HowToRegIcon />
                  </ListItemIcon>
                  <ListItemText primary="Access Groups" />
                </ListItem>
              </Link>
            </List>
          </Collapse>
          <Divider light />

          <Link to="/notifications">
            <ListItem
              button
              selected={
                currentPathName.includes("/notification") ? true : false
              }
              className={clsx(
                classes.list_item,
                "/notifications" === currentPathName
                  ? classes.list_item_active
                  : null
              )}
            >
              <ListItemIcon>
                <NotificationsActiveIcon
                  color={
                    currentPathName.includes("/notification")
                      ? "primary"
                      : "secondary"
                  }
                />
              </ListItemIcon>
              <Hidden only={["sm", "xs", "md"]}>
                <ListItemText>
                  <Typography color="textSecondary" variant="button">
                    Notifications
                  </Typography>
                </ListItemText>
              </Hidden>
            </ListItem>
          </Link>

          {/* <Link to="/manage">
            <ListItem
              button
              selected={currentPathName.includes("/manage") ? true : false}
              className={clsx(
                classes.list_item,
                currentPathName.includes("/manage")
                  ? classes.list_item_active
                  : null
              )}
            >
              <ListItemIcon>
                <SettingsIcon
                  color={
                    currentPathName.includes("/manage")
                      ? "primary"
                      : "secondary"
                  }
                />
              </ListItemIcon>
              <Hidden only={["sm", "xs", "md"]}>
                <ListItemText>
                  <Typography color="textSecondary" variant="button">
                    Settings
                  </Typography>
                </ListItemText>
              </Hidden>
            </ListItem>
          </Link>
        */}
        </List>
      </Box>

      <Box px={3}>
        <Logout />
      </Box>
    </Paper>
  );
}

export default Sidebar;
