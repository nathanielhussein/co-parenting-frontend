import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "calc(100vh - 70px)",
    padding: "10px 0px",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
  },
  upper_menu: {
    // border: "1px solid red",
  },
  lower_menu: {
    // border: "1px solid red",
  },
  list_item: {
    fontWeight: "bold",
    fontSize: "20px",
  },
  list_item_active: {
    // backgroundColor: "rgba(28, 173, 40, 0.3)",
    color: "#30637a",
    borderLeft: "7px solid #30637a",
  },
  nested: {
    paddingLeft: "40px !important",
  },
  menulists: {
    width: "100%",
  },
  sidebar_action: {
    display: "flex",
  },
  logoutBtn: {
    padding: "10px 0px",
    backgroundColor: "rgba(28, 173, 40, 0.3)",
    textAlign: "center",
    display: "flex",
    justifyContent: "center",
    textTransform: "none !important",

    "&:hover": {
      border: "1px solid #30637a",
      borderRadius: "10px",
    },
  },
}));

export default useStyles;
