import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    maxHeight: "70px",
    boxShadow: "0",
  },
  toolbar: {
    display: "flex",
    justifyContent: "space-between",
  },
  nav_items: {
    cursor: "pointer",
    display: "flex",
    alignItems: "center",
  },

}));
export default useStyles