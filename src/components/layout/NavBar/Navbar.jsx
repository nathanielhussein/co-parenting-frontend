import {
  AppBar,
  Avatar,
  Box,
  IconButton,
  Menu,
  MenuItem,
  Paper,
  Toolbar,
  Typography,
} from "@material-ui/core";
import Logout from "components/Modals/Logout/Logout";

import { useState } from "react";
import { Link } from "react-router-dom";
import { useAuthState } from "context";
import useStyles from "./nav_style";
import PersonIcon from "@material-ui/icons/Person";
function Navbar() {
  const classes = useStyles();
  const { auth_user } = useAuthState(); //read user details from context
  const [isOpen, setIsOpen] = useState(null);
  const handleClick = (event) => {
    setIsOpen(event.currentTarget);
  };

  const handleClose = () => {
    setIsOpen(null);
  };

  return (
    <AppBar
      elevation={0}
      className={classes.root}
      position="static"
      color="transparent"
    >
      <Paper>
        <Toolbar className={classes.toolbar}>
          <Link to="/">
            <IconButton
              edge="start"
              className={classes.nav_brand}
              color="inherit"
              aria-label="menu"
            >
              <img src="/images/logo.svg" alt="logo" width={50} height={50} />
            </IconButton>
          </Link>
          {auth_user && (
            <div className={classes.nav_items}>
              <Box display="flex" alignItems="center" mx={2}>
                <Box mx={1}>
                  <Typography
                    className={classes.nav_item}
                    variant="body1"
                    component="h2"
                    color="primary"
                  >
                    {auth_user.email}
                  </Typography>
                </Box>

                <Avatar
                  alt="profile Photo"
                  src="/images/avatar.jpeg"
                  onClick={handleClick}
                  aria-controls="menu-appbar"
                  aria-haspopup="true"
                >
                  N
                </Avatar>
              </Box>
              <Menu
                id="simple-menu"
                anchorEl={isOpen}
                keepMounted
                open={Boolean(isOpen)}
                onClose={handleClose}
                elevation={0}
                getContentAnchorEl={null}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "center",
                }}
                transformOrigin={{
                  vertical: "top",
                  horizontal: "center",
                }}
              >
                <Link to="/profile">
                  <MenuItem onClick={handleClose}>
                    <Box display="flex" alignItems="center">
                      <Box mr={2}>
                        <PersonIcon color="primary" />
                      </Box>
                      <Typography color="primary">Profile</Typography>
                    </Box>
                  </MenuItem>
                </Link>
                <Logout isFromNav={true} />
              </Menu>
            </div>
          )}
        </Toolbar>
      </Paper>
    </AppBar>
  );
}

export default Navbar;
