import { ThemeProvider } from '@material-ui/styles';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import theme from './theme';
import CssBaseline from "@material-ui/core/CssBaseline";
import { Helmet } from 'react-helmet';


ReactDOM.render(
  <ThemeProvider theme={theme}>
    <CssBaseline />
    <Helmet>
      <meta charSet="utf-8" />
      <title>Co-parenting</title>
    </Helmet>
    <App />
  </ThemeProvider>,
  document.getElementById("root")
);
