import { format, fromUnixTime, parseISO } from "date-fns";

export const 
getCreatedDate = (date) => {
  let result = "";
  result = format(
    parseISO(new Date(date).toISOString()),
    "MM-dd-yy",
    new Date()
  );
  console.log("created At", result);
  return result;
};

export const getDateFromUnix = (date) => {
  let result = "";
  result = format(fromUnixTime(date), "MM-dd-yy", new Date());
  return result;
};
