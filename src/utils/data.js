const data = [
  {
    name: "",
    uv: 0,
    pv: 0,
    amt: 0,
  },
  {
    name: "Week 1",
    uv: 1900,
    pv: 1400,
    amt: 1400,
  },
  {
    name: "Week 2",
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: "Week 3",
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: "Week 4",
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: "Week 5",
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: "Week 6",
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: "Week 7",
    uv: 2490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: "Week 8",
    uv: 3490,
    pv: 4000,
    amt: 2100,
  },
  {
    name: "Week 9",
    uv: 3190,
    pv: 2300,
    amt: 3100,
  },
];

export default data;
