import React from "react";
import BaseLayout from "../../components/layout/BaseLayout/BaseLayout";
import NotifyUsers from "../../components/modules/NotifyUsers/NotifyUsers";

function NotifyUsersPage() {
  return (
    <BaseLayout>
      <NotifyUsers />
    </BaseLayout>
  );
}

export default NotifyUsersPage;
