import React from "react";
import { Helmet } from "react-helmet";
import BaseLayout from "../../components/layout/BaseLayout/BaseLayout";
import Team from "../../components/modules/Team/Team";

function TeamPage() {
  return (
    <BaseLayout>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Co-parenting|Team</title>
      </Helmet>
      <Team />
    </BaseLayout>
  );
}

export default TeamPage;
