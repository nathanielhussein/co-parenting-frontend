import React from 'react'
import BaseLayout from '../../components/layout/BaseLayout/BaseLayout'
import Notications from '../../components/modules/Notifications/Notifications'

function NotificationsPage() {
    return (
        <BaseLayout>
            <Notications/>
        </BaseLayout>
    )
}

export default NotificationsPage
