import React from "react";
import Custom404 from "../../components/errors/Custom404/Custom404";

const PageNotFound = () => {
  return <Custom404 />;
};
export default PageNotFound;
