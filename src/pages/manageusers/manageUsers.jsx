import React from "react";
import BaseLayout from "../../components/layout/BaseLayout/BaseLayout";
import ManageUsers from "../../components/modules/ManageUsers/ManageUsers";

function ManageUsersPage() {
  return (
    <BaseLayout>
      <ManageUsers />
    </BaseLayout>
  );
}

export default ManageUsersPage;
