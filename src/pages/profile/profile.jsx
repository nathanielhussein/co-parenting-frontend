import { Grid } from "@material-ui/core";
import { getUserData } from "api/users";
import { NormalLoader } from "components/Loaders/Loader";
import { useAuthState } from "context";
import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import BaseLayout from "../../components/layout/BaseLayout/BaseLayout";
import Profile from "../../components/modules/Profile/Profile";

function ProfilePage() {
  const { auth_user } = useAuthState(); //read user details from context
  const [loggedUser, setLoggedUser] = useState(auth_user);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchUser = async () => {
      setLoading(true);
      const data = await getUserData(auth_user.id);
      setLoggedUser(data);
      setLoading(false);
    };
    fetchUser();
  }, [auth_user.id]);
  return (
    <div>
      <BaseLayout>
        <Helmet>
          <meta charSet="utf-8" />
          <title>Co-parenting|Profile</title>
        </Helmet>
        {loading ? (
          <Grid container justify="center" alignItems="center" style={{minHeight: "80vh"}}>
            <NormalLoader />
          </Grid>
        ) : (
          <Profile userData={loggedUser} />
        )}
      </BaseLayout>
    </div>
  );
}

export default ProfilePage;
