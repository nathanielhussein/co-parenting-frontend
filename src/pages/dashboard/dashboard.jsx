import React from "react";
import BaseLayout from "../../components/layout/BaseLayout/BaseLayout";
import Dashboard from "../../components/modules/Dashboard/Dashboard";
import {Helmet} from "react-helmet";

function DashboardPage() {
  return (
    <BaseLayout>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Dashboard</title>
      </Helmet>
      <Dashboard />
    </BaseLayout>
  );
}

export default DashboardPage;
