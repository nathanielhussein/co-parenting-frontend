import React from "react";
import { Route, Switch, useRouteMatch } from "react-router-dom";
import BaseLayout from "../../components/layout/BaseLayout/BaseLayout";
import Users from "../../components/modules/Users/Users";
import SingleUserPage from "./singleuser";
import { Helmet } from "react-helmet";

function UsersPage() {
  let { path } = useRouteMatch();

  return (
    <BaseLayout>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Co-parenting|Users</title>
      </Helmet>
      <Switch>
        <Route exact path={`${path}`}>
          <Users />
        </Route>
        <Route path={`${path}/:id`}>
          <SingleUserPage />
        </Route>
      </Switch>
    </BaseLayout>
  );
}

export default UsersPage;
