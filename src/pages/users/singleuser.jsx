import React from "react";
import { Helmet } from "react-helmet";
import { useParams } from "react-router";
import SingleUser from "../../components/modules/Users/SingleUser/SingleUser";

function SingleUserPage() {
  let { id } = useParams();
  return (
    <React.Fragment>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Users</title>
      </Helmet>
      <SingleUser id={id} />;
    </React.Fragment>
  );
}

export default SingleUserPage;
