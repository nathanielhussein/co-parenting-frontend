import React from "react";
import ResetPassword from "../../components/modules/Auth/ResetPassword/ResetPassword";

function ResetPasswordPage() {
  return (
    <div>
      <ResetPassword />
    </div>
  );
}

export default ResetPasswordPage;
