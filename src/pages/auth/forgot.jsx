import React from "react";
import ForgotPassword from "../../components/modules/Auth/ForgotPassword/ForgotPassword";

function ForgotPasswordPage() {
  return (
    <div>
      <ForgotPassword />
    </div>
  );
}

export default ForgotPasswordPage;
