import React, { Suspense } from "react";
import { Route, Switch, BrowserRouter, Redirect } from "react-router-dom";
import { GlobalLoader } from "./components/Loaders/Loader";
import { AuthProvider } from "./context";
import PageNotFound from "./pages/404/404";
import ForgotPasswordPage from "./pages/auth/forgot";
import ResetPasswordPage from "./pages/auth/reset";
import PrivateRoute from "./RouteHandler/PrivateRoute";

const LoginPage = React.lazy(() => import("./pages/auth/login"));
const DashboardPage = React.lazy(() => import("./pages/dashboard/dashboard"));
const ManageUsersPage = React.lazy(() =>
  import("./pages/manageusers/manageUsers")
);
const NotificationsPage = React.lazy(() =>
  import("./pages/notifications/notifications")
);
const TeamPage = React.lazy(() => import("./pages/team/team"));
const UsersPage = React.lazy(() => import("./pages/users/users"));
const ProfilePage = React.lazy(() => import("./pages/profile/profile"));

function Router() {
  return (
    <AuthProvider>
      <BrowserRouter>
        <Suspense fallback={<GlobalLoader />}>
          <Switch>
            <PrivateRoute path="/" exact>
              <Redirect to="/dashboard" />
            </PrivateRoute>
            <Route path="/auth/login" exact component={LoginPage} />
            <Route path="/auth/reset" exact component={ResetPasswordPage} />
            <Route path="/auth/forgot" exact component={ForgotPasswordPage} />
            <PrivateRoute path="/dashboard" component={DashboardPage} />
            <PrivateRoute path="/users" component={UsersPage} />
            <PrivateRoute path="/team" component={TeamPage} />
            <PrivateRoute path="/manage" component={ManageUsersPage} />
            <PrivateRoute path="/notifications" component={NotificationsPage} />
            <PrivateRoute path="/profile" component={ProfilePage} />
            <Route path="*" component={PageNotFound} />
          </Switch>
        </Suspense>
      </BrowserRouter>
    </AuthProvider>

    
  );
}

export default Router;
