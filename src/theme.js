import { createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
  overrides: {
    MuiPaper: {
      root: {
      },
    },
    MuiListItem: {
      root: {
        padding: "15px 30px !important",
        "&$selected": {
          backgroundColor: "rgba(48, 99, 122, 0.2)",
        },
        "&$hovered": {
          backgroundColor: "rgba(48, 99, 122, 0.2)",
        },
      },
    },
  },
  palette: {
    background: {
      default: "rgb(240, 240, 240)",
    },
    primary: {
      light: "#30637a",
      main: "#30637a",
      dark: "#046651",
      contrastText: "#fff",
    },
    secondary: {
      main: "#ccc",
    },
    neutral: {
      main: "#5c6ac4",
    },
    // text: {
    //   primary: {
    //     light: "rgba(48, 99, 122, 0.2)",
    //     main: "rgba(48, 99, 122, 0.2)",
    //   },
    //   secondary: {
    //     light: "rgba(0, 0, 0, 0.4)",
    //     main: "rgba(0, 0, 0, 0.4)",
    //   },
    // },
  },
});

export default theme;
