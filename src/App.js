import { Helmet } from 'react-helmet';
import './App.css';
import Router from './Routes';

function App() {
  return (
    <div className="App">
      <Helmet>
        <meta charSet="utf-8" />
        <title>Co-parenting</title>
      </Helmet>
      <Router />
    </div>
  );
}

export default App;
